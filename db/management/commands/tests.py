from django.core.management.base import BaseCommand
import unittest
from db.tests import test_all, test_performance

class Command(BaseCommand):
    def handle(self, **options):
        suite = unittest.TestLoader().loadTestsFromTestCase(TestAll)
        unittest.TextTestRunner().run(suite)

class TestAll(unittest.TestCase):
    def test_all(self):
        test_all()

    def test_performance(self):
        test_performance()
