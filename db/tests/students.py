#-*- coding : utf-8 -*-
from .utils import get_from_file, store_to_file
from db.models import Student

def get_students(year=0, only_under=True):
    try:
        if year != 0:
            students = get_from_file('students{0}_{1}'.format('_only_under' if only_under else '', year))
        else:
            students = get_from_file('students{0}'.format('_only_under' if only_under else ''))
    except IOError:
        if year != 0:
            if only_under:
                students = Student.objects.filter(year__lte=year)
            else:
                students = Student.objects.all()
            store_to_file(students, 'students{0}_{1}'.format('_only_under' if only_under else '', year))
        else:
            students = Student.objects.all()
            store_to_file(students, 'students{0}'.format('_only_under' if only_under else ''))

    return students
