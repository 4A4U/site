#-*- coding: utf-8 -*-
from ..tests import get_from_file, store_to_file, get_formations, get_students, get_distance, get_distance_exponent
from db.models import Student

def get_score(e_distance = 0, e_students = 0, year = 0, distance = 'jaccard', only_under = True, students = [], formations = [], distance_matrix = [], all_e = False): #, coef=0.8, rank=30 #unused args
    try:
        if len(students) != 0 or len(formations) != 0:
            all_elements = all_e
            if not all_elements:
                raise IOError()
        all_elements = True
        if year == 0:
            score_matrix = get_from_file('{0}/{4}/eDistance={1}/eStudents={2}/score_{0}_eDistance={1}_eStudents={2}{3}'.format(distance, e_distance, e_students, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))
        else:
            score_matrix = get_from_file('{0}/{5}/eDistance={1}/eStudents={2}/score_{0}_eDistance={1}_eStudents={2}_{4}{3}'.format(distance, e_distance, e_students, year, 'only_under_' if only_under else '', 'only_under' if only_under else 'not_only_under'))
    except IOError:
        if len(formations) == 0:
            formations = get_formations()
        if len(students) == 0:
            students = get_students(year, only_under)
            all_students = True
        else:
            all_students = False
        if len(distance_matrix) == 0:
            distance_matrix = get_distance(distance, year, only_under, students, all_students)
        distance_matrix_exponent = get_distance_exponent(e_distance, distance, year, only_under, distance_matrix, all_students)
        score_matrix = [[0 for x in range(len(formations))] for y in range(len(students))]
        print('begin score computation')
        for formation in formations:
            year_list = Student.objects.values_list('year', flat=True).distinct()
            choices = {}
            for y in year_list:
                if only_under:
                    c = formation.studentchoice_set.filter(student__year__lt=y)
                    choices[y] = {'c': c, 'l': len(c) ** e_students}
                else:
                    c = formation.studentchoice_set.exclude(student__year=y)
                    choices[y] = {'c': c, 'l': len(c) ** e_students}
            for stud in students:
                for studentchoice in choices[stud.year]['c']:
                    coeff = studentchoice.ratio / choices[stud.year]['l']
                    score_matrix[stud.id-1][formation.id-1] +=  coeff * (distance_matrix_exponent[stud.id-1][studentchoice.student.id-1])
            if formation.id % 100 == 0:
                print("100 formations done")
        if all_elements:
            if year != 0:
                store_to_file(score_matrix, '{0}/{5}/eDistance={1}/eStudents={2}/score_{0}_eDistance={1}_eStudents={2}_{4}{3}'.format(distance, e_distance, e_students, year, 'only_under_' if only_under else '', 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(score_matrix, '{0}/{4}/eDistance={1}/eStudents={2}/score_{0}_eDistance={1}_eStudents={2}{3}'.format(distance, e_distance, e_students, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))
    return score_matrix
