#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file, get_distance

def get_distance_exponent(distance_exponent = 0, distance = 'jaccard', year = 0, only_under = True, distance_matrix = [], all_e = False):
    try:
        if len(distance_matrix) != 0:
            all_elements = all_e
            if not all_elements:
                raise IOError()
        all_elements = True
        if year != 0:
            distance_matrix_exponent = get_from_file('{0}/{4}/eDistance={3}/distance_exponent_{0}_eDistance={3}_{2}{1}'.format(distance, year, 'only_under_' if only_under else '', distance_exponent, 'only_under' if only_under else 'not_only_under'))
        else:
            distance_matrix_exponent = get_from_file('{0}/{3}/eDistance={2}/distance_exponent_{0}_eDistance={2}{1}'.format(distance, '_only_under' if only_under else '', distance_exponent, 'only_under' if only_under else 'not_only_under'))
    except IOError:
        if len(distance_matrix) == 0:
            distance_matrix = get_distance(distance, year, only_under)
        print('begin distance_exponent computation')
        distance_matrix_exponent = [[element ** distance_exponent for element in row]for row in distance_matrix]
        if all_elements:
            if year != 0:
                store_to_file(distance_matrix_exponent, '{0}/{4}/eDistance={3}/distance_exponent_{0}_eDistance={3}_{2}{1}'.format(distance, year, 'only_under_' if only_under else '', distance_exponent, 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(distance_matrix_exponent, '{0}/{3}/eDistance={2}/distance_exponent_{0}_eDistance={2}{1}'.format(distance, '_only_under' if only_under else '', distance_exponent, 'only_under' if only_under else 'not_only_under'))
    return distance_matrix_exponent

