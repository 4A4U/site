#-*- coding : utf-8 -*-
import pickle
import decimal
from statistics import mean

def get_from_file(file_name):
    with open('DataDump/{0}.txt'.format(file_name), 'rb') as fp:
        value = pickle.load(fp)
        #print('{0} imported'.format(file_name))
        return value

def store_to_file(value, file_name):
    with open('DataDump/{0}.txt'.format(file_name), 'wb') as fp:
        pickle.dump(value, fp)
        print('{0} saved'.format(file_name))

def avg(x):
    x = filter(None, x)
    return  mean(x)


def drange(x, y, jump):
    i = x
    while i < y:
        yield round(i, 3)
        i += jump

    #return [x + i*jump for i in range((y-x)/jump)]
