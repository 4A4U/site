from .utils import get_from_file, store_to_file, avg, drange
#TODO: parse each time for year = 0

from .formations import get_formations #args: None
from .students import get_students #args: year = 0, only_under=True
from .studentsChoice import get_studentsChoice #args: year=0, only_under=True

from .formations_similarity import get_formations_similarity #args: formations
from .formation_total import get_formation_total #args: formations_similarity

from .distance import get_distance #args: distance='jaccard', year=0, only_under=True, students=[], all_e=False
from .distance_exponent import get_distance_exponent
from .score import get_score #args: e_distance = 0, e_students = 0, year = 0, distance = 'jaccard', only_under = True, students = [], formations = [], distance_matrix = [], all_e = False
from .position import get_position #args: e_distance = 0, e_students = 0, opt = '', distance = 'jaccard', year = 0, only_under = True, students = [], formations = [], score_matrix = [], all_e = False #TODO: add , year

from .avg_pos_order import get_avg_pos_order # args: score_matrix, distance_exponent, students_exponent, year, distance, only_under=True
from .pos_order import get_pos_order #args: score_matrix = [], distance_exponent = 0, students_exponent = 0, year = 0, distance = 'jaccard', only_under = True, all_e = False

from .performance import get_performance #args: rank, e_distance = 0, e_students = 0, distance = 'jaccard', year = 0, only_under = True, position_matrix = [], all_e = False
from .performance_score import get_performance_score #args: coeff = 0.8, rank = 30, distance_exponent = 0, students_exponent = 0, distance = 'jaccard', year = 0, only_under = True, performance = [], all_e = False

#from .student_score import get_student_score #args: student, opt='_2' TODO: remove call to compute_score
from .api import return_all #args: functions, year=0, only_under=True, e_distance=[], e_students=[], coef=[], rank=[],students=[], studentsChoice=[], formations=[], score=[[]], position=[[]], distance='all', needed={}
from .gen_get import gen_get

from .compare_Xponents import compare_Xponents #args: None
from .test import test_all, test_performance #args: opt1='', opt2='', opt3='', e_distance=0, e_students=0
