from db.models import Student, Formation, StudentChoice
import pickle
from itertools import zip_longest
from statistics import mean
import numpy as np
import decimal
from ..tests import get_students, get_position, get_studentsChoice, get_distance, get_score, avg, drange, get_from_file, store_to_file, get_formations, get_formations_similarity, get_formation_total, get_pos_order, get_performance, get_performance_score

def return_all(functions, year=0, only_under=True, e_distance=[], e_students=[], coeffs=[], ranks=[], students=[], studentsChoice=[], formations=[], score=[[]], position=[[]], distances='all', needed=set()):
    print('begin return_all')
    if distances == 'all':
        distances = ['jaccard', 'hamming']
    #name = 'compute_'
    #distance_dependant = ['distance', 'performance', 'position']
    #exponent_dependant = ['score', 'position', 'performance']
    #error_dependant = ['performance_score', 'score']
    needs = {
        'students': [],
        'formations': [],
        'studentsChoice': [],
        'distance': ['students'],
        'formations_similarity': ['formations'],
        'formations_total': ['formations', 'formations_similarity'],
        'score': ['formations', 'students', 'distance'],
        'pos_order': ['formations', 'students', 'distance', 'score'],
        'position': ['students', 'formations', 'distance', 'score'],
        'performance': ['students', 'formations', 'distance', 'score', 'position'],
        'performance_score': ['students', 'formations', 'distance', 'score', 'position', 'performance']
    }

    if functions == 'all':
        needed = {'students', 'formations', 'distance', 'score', 'position', 'performance', 'formations_similarity', 'formations_total', 'pos_order', 'performance_score'}
        functions = []

    for function in functions:
        needed.add(function)
        needed.union(set(needs[function]))

    #variables = {'students': students,'formations': formations,'studentsChoice': studentsChoice, 'score_matrix': score, 'position_matrix': position}
    #opt_name = ''

    result = {}
    if 'formations' in needed:
        print('formation needed')
        formations = get_formations()
        needed.remove('formations')
        result['formations'] = formations
        print('formation computed')

    if 'students' in needed:
        print('students needed')
        needed.remove('students')
        students = get_students(year, only_under)
        result['students'] = students
        print('students computed')

    if 'studentsChoice' in needed:
        print('studentsChoice needed')
        needed.remove('studentsChoice')
        studentsChoice = get_studentsChoice(year, only_under)
        result['studentsChoice'] = studentsChoice

    if 'formations_similarity' in needed:
        print('formations_similarity needed')
        needed.remove('formations_similarity')
        formations_similarity = get_formations_similarity(formations)
        result['formations_similarity'] = formations_similarity

    if 'formations_total' in needed:
        print('formations_total needed')
        needed.remove('formations_total')
        formations_total = get_formation_total(formations_similarity)
        result['formations_total'] = formations_total

    if 'distance' in needed:
        print('distance needed')
        needed.remove('distance')
        for distance in distances:
            print('distance : {0}'.format(distance))
            result_for_distance = {}
            distance_matrix = get_distance(distance, year, only_under, students, True)
            result_for_distance['distance'] = distance_matrix

            if 'score' in needed:
                print('score needed')
                for distance_exponent in e_distance:
                    print('distance_exponent : {0}'.format(distance_exponent))
                    result_for_distance_exponents = {}
                    for students_exponent in e_students:
                        print('students_exponent : {0}'.format(students_exponent))
                        result_for_students_exponents = {}
                        score_matrix = get_score(distance_exponent, students_exponent, year, distance, only_under, students, formations, distance_matrix, True)
                        print('score computed')

                        result_for_students_exponents['score'] = score_matrix
                        if 'pos_order' in needed:
                            print('pos_order needed')
                            pos_order = get_pos_order(score_matrix, distance_exponent, students_exponent, year, distance, only_under, True)
                            result_for_students_exponents['pos_order'] = pos_order

                        if 'position' in needed:
                            print('position needed')
                            position_matrix = get_position(distance_exponent, students_exponent, distance = distance, year = year, only_under = only_under, students = students, formations = formations, score_matrix = score_matrix, all_e = True)
                            result_for_students_exponents['position'] = position_matrix

                            if 'performance' in needed:
                                print('performance needed')
                                result_for_students_exponents['ranks'] = {}
                                for rank in ranks:
                                    print('rank : {0}'.format(rank))
                                    result_for_rank = {}
                                    performance = get_performance(rank, distance_exponent, students_exponent, distance, year, only_under, position_matrix, True)
                                    print('performance computed')
                                    result_for_rank['performance'] = performance

                                    if 'performance_score' in needed:
                                        print('performance_score needed')
                                        result_for_rank['performance_score'] = {}
                                        for coeff in coeffs:
                                            print('coeff : {0}'.format(coeff))
                                            performance_score = get_performance_score(coeff, rank, distance_exponent, students_exponent, distance, year, only_under, performance, True)
                                            result_for_rank['performance_score'][coeff] = performance_score
                                            print('performance_score computed')
                                    result_for_students_exponents['ranks'][rank] = result_for_rank
                    result_for_distance_exponents[students_exponent] = result_for_students_exponents
                result_for_distance[distance_exponent] = result_for_distance_exponents
            result[distance] = result_for_distance

   # if year != 0:
   #     opt_name += '_year={0}'.format(year)
   # for func_name in functions:#for each function
   #     loopvars = [[''], [''], [''], ['']]
   #     if func_name in exponent_dependant:#add lists of exponents if exponent_dependant
   #         loopvars[0] = e_distance
   #         loopvars[1] = e_students
   #     if func_name in error_dependant:#add lists of coef and rank if error_dependant
   #         loopvars[2] = coef
   #         loopvars[3] = rank
   #     for var1 in loopvars[0]:#for each distance exponent
   #         for var2 in loopvars[1]:
   #             for var3 in loopvars[2]:
   #                 for var4 in loopvars[3]:
   #                     if func_name in exponent_dependant:
   #                         opt_name += '_eDistance={0}_eStudents={1}'.format(e_distance, e_students)
   #                     if func_name in error_dependant:
   #                         opt_name += '_rank={0}_coef={1}'.format(rank, coef)
   #                     try:
   #                         variable[func_name] = get_from_file('{}'.format(func_name+opt_name))
   #                         print('{} imported'.format(func_name+opt_name))
   #                     except IOError:
   #                         variable[func_name] = locals()[name+func_name](var1, var2, variable)
   #                         store_to_file(variable[func_name], '{}'.format(func_name+opt_name))

    return result
