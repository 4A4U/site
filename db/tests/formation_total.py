#-*- coding : utf-8 -*-

def get_formation_total(formations_similarity):
    return [formations_similarity[i][i] for i in range(len(formations_similarity))]
