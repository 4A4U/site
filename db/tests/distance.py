#-*- coding : utf-8 -*-
from functools import reduce
from ..tests import get_from_file, store_to_file

def get_distance(distance = 'jaccard', year = 0, only_under = True, students = [], all_e = False):
    try:
        if len(students) != 0:
            all_students = all_e
            if not all_students:
                raise IOError()
        all_students = True
        if year != 0:
            distance_matrix = get_from_file('{0}/distance_{0}_{2}{1}'.format(distance, year, 'only_under_' if only_under else ''))
        else:
            distance_matrix = get_from_file('{0}/distance_{0}{1}'.format(distance, '_only_under' if only_under else ''))
    except IOError:
        print('begin distance computation')
        if distance == 'jaccard':
            distance_matrix = [[intersection(u.courses, v.courses) / (bin(u.courses).count('1') + bin(u.courses).count('1')  - intersection(u.courses, v.courses)) for u in students ] for v in students]
        elif distance == 'hamming':
            distance_matrix = [[xor(u.courses, v.courses) for u in students] for v in students]
        else:
            raise ValueError("Available distance are 'jaccard' and 'hamming'.")

        if all_students:
            if year != 0:
                store_to_file(distance_matrix, '{0}/distance_{0}_{2}{1}'.format(distance, year, 'only_under_' if only_under else ''))
            else:
                store_to_file(distance_matrix, '{0}/distance_{0}{1}'.format(distance, '_only_under' if only_under else ''))
    return distance_matrix

def intersection(bin1,bin2):
    return reduce(lambda x,y: x+(int(y[0])&int(y[1])), list(zip(bin1,bin2)[2:]))

def xor(bin1,bin2):
    return reduce(lambda x,y: x+(int(y[0])^int(y[1])), list(zip(bin1,bin2)[2:]))
