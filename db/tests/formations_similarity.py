#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file

def get_formations_similarity(formations = [], all_e = False):
    try:
        if len(formations) != 0:
            all_formations = all_e
            if not all_formations:
                raise IOError()
        all_formations = True
        formationSimilarity = get_from_file('formationSimilarity')
    except IOError:
        formationLength = len(formations)
        #studentsChoice = get_studentsChoice()
        #choiceByStudent = [[] for f in range(len(get_students()))]
        #choiceByStudent =  [student.studentchoice_set.values_list('formation', flat=True) for student in get_students()]
        #for choice in studentsChoice:
        #    choiceByStudent[choice.student_id-1].append(choice.formation_id)
        formationSimilarity = [[0 for i in range(formationLength)] for j in range(formationLength)]
        #studentsByFormation = [formation.studentchoice_set.values_list('student', flat=True) for formation in get_formations()]
        formationNearFormation = [formation.studentchoice_set.values_list('student__studentchoice__formation', flat=True) for formation in formations]
        i = 0
        for formations2 in formationNearFormation:
            for formation in formations2:
                formationSimilarity[i][formation-1] += 1
            i += 1

        #for choices in choiceByStudent:#pour chaque étudiant
        #    for formation1 in choices:
        #        for formation2 in choices :
        #            formationSimilarity[formation1-1][formation2-1]+=1 # if two formations have been chosen by the same student, add 1
        if all_formations:
            store_to_file(formationSimilarity, 'formationSimilarity')

    return formationSimilarity
