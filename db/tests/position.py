#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file, get_students, get_formations, get_score

def get_position(e_distance = 0, e_students = 0, opt = '', distance = 'jaccard', year = 0, only_under = True, students = [], formations = [], score_matrix = [], all_e = False):
    try:
        len_s = len(students)
        len_f = len(formations)
        len_sm = len(score_matrix)

        if len_s != 0 or len_f != 0 or len_sm != 0:
            all_elements = all_e
            if not all_elements:
                raise IOError()

        all_elements = True
        if year == 0:
            position_matrix = get_from_file('{3}/{5}/eDistance={1}/eStudents={2}/position{0}_{3}_eDistance={1}_eStudents={2}{4}'.format(opt, e_distance, e_students, distance, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))
        else:
            position_matrix = get_from_file('{3}/{6}/eDistance={1}/eStudents={2}/position{0}_{3}_eDistance={1}_eStudents={2}{4}_{5}'.format(opt, e_distance, e_students, distance, '_only_under' if only_under else '', year, 'only_under' if only_under else 'not_only_under'))

    except IOError:
        if len_s == 0:
            students = get_students(year, only_under)
        if len_f == 0:
            formations = get_formations()
        if len_sm == 0:
            score_matrix == get_score(e_distance, e_students, year, distance, only_under, students, formations, all_e = all_elements)
        position_matrix  = [ [ sorted(formations, key=lambda a: score_matrix[stud.id-1][a.id-1],reverse=True).index(choice.formation) for choice in stud.studentchoice_set.order_by('order').all()] for stud in students]

        if all_elements:
            if year == 0:
                store_to_file(position_matrix, '{3}/{5}/eDistance={1}/eStudents={2}/position{0}_{3}_eDistance={1}_eStudents={2}{4}'.format(opt, e_distance, e_students, distance, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(position_matrix, '{3}/{6}/eDistance={1}/eStudents={2}/position{0}_{3}_eDistance={1}_eStudents={2}{4}_{5}'.format(opt, e_distance, e_students, distance, '_only_under' if only_under else '', year, 'only_under' if only_under else 'not_only_under'))

    return position_matrix
