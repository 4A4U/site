#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file, get_performance

def get_performance_score(coeff = 0.8, rank = 30, distance_exponent = 0, students_exponent = 0, distance = 'jaccard', year = 0, only_under = True, performance = [], all_e = False):
    try:
        if len(performance) != 0:
            all_elements = all_e
            if not all_elements:
                raise IOError()
        all_elements = True
        if year == 0:
            performance_score = get_from_file('{2}/{6}/eDistance={0}/eStudents={1}/rank={4}/coeff={5}/performance_score_{2}_coeff={5}_rank={4}_eDistance={0}_eStudents={1}{3}'.format(distance_exponent, students_exponent, distance, '_only_under' if only_under else '', rank, coeff, 'only_under' if only_under else 'not_only_under'))
        else:
            performance_score = get_from_file('{2}/{7}/eDistance={0}/eStudents={1}/rank={5}/coeff={6}/performance_score_{2}_coeff={6}_rank={5}_eDistance={0}_eStudents={1}{3}_{4}'.format(distance_exponent, students_exponent, distance, '_only_under' if only_under else '', year, rank, coeff, 'only_under' if only_under else 'not_only_under'))
    except IOError:
        if len(performance) == 0:
            performance = get_performance(rank, distance_exponent, students_exponent, distance, year, only_under, all_e = True)
        performance_score = 0
        print(len(performance))
        for i in range(len(performance)):
            performance_score += performance[i][rank - 1] * (coeff ** i)

        if all_elements:
            if year == 0:
                store_to_file(performance_score, '{2}/{6}/eDistance={0}/eStudents={1}/rank={4}/coeff={5}/performance_score_{2}_coeff={5}_rank={4}_eDistance={0}_eStudents={1}{3}'.format(distance_exponent, students_exponent, distance, '_only_under' if only_under else '', rank, coeff, 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(performance_score, '{2}/{7}/eDistance={0}/eStudents={1}/rank={5}/coeff={6}/performance_score_{2}_coeff={6}_rank={5}_eDistance={0}_eStudents={1}{3}_{4}'.format(distance_exponent, students_exponent, distance, '_only_under' if only_under else '', year, rank, coeff, 'only_under' if only_under else 'not_only_under'))

    return performance_score
