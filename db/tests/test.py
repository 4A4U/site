#-*- coding: utf-8 -*-
from ..tests import gen_get, drange, return_all #get_students, get_studentsChoice, get_formations, get_distance, get_score, get_position, get_performance, avg, avg_pos_order
from itertools import zip_longest

def test_all(opt1='',opt2='',opt3='', e_distance=0, e_students=0):
    print('Beginning test')

    for b in [False, True]:
        for d in list(range(0, 10)):
            for s in list(drange(0, 1, 0.05)):
                gen_get(['performance_score'], 0, b, [d], [s], list(drange(0.5, 1, 0.1)), list(range(10, 50, 10)))
        #gen_get('all', 0, True, list(range(0, 10)), list(drange(0, 1, 0.05)), list(drange(0.5, 1, 0.1)), list(range(10, 50, 10)))

    #avg_pos_order = map(avg, zip(*[[i[0] for i in sorted(enumerate(score_student), key=lambda x:x[1],reverse=True)] for score_student in score_matrix]))
    #[np.percentile(np.array(list(filter(None,row))), np.arange(10,100,10)) for row in zip_longest(*position_matrix)]

    #return performance

def test_performance():
    for b in [False, True]:
        for coeff in list(drange(0.5, 1, 0.1)):
            for rank in list(range(10, 50, 10)):
                ed_max = 0
                es_max = 0
                d_max = 'jaccard'
                p_max = 0
                for ed in list(range(0, 20)):
                    for es in list(drange(0, 1, 0.05)):
                        for d in ['hamming', 'jaccard']:
                            p = gen_get(['performance_score'], 0, b, [ed], [es], [coeff], [rank], [d])
                            if p[d][ed][es][rank]['performance_score'][coeff] > p_max:
                                p_max = p[d][ed][es][rank]['performance_score'][coeff]
                                d_max = d
                                es_max = es
                                ed_max = ed
                print("{0}, for rank={1} and coeff={2}, best performance ({3}) reach for distance {4}, with exponents {5} for distance and {6} for students.".format("Regarding only year under the one of the student" if b else "Regarding all years", rank, coeff, p_max, d_max, ed_max, es_max))
