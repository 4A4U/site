#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file, get_score

def get_pos_order(score_matrix = [], distance_exponent = 0, students_exponent = 0, year = 0, distance = 'jaccard', only_under = True, all_e = False):
    try:
        if len(score_matrix) != 0:
            all_elements = all_e
            raise IOError()
        all_elements = True

        if year == 0:
            pos_order = get_from_file('{0}/{4}/eDistance={1}/eStudents={2}/pos_order_{0}_eDistance={1}_eStudents={2}{3}'.format(distance, distance_exponent, students_exponent, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))
        else:
            pos_order = get_from_file('{0}/{5}/eDistance={1}/eStudents={2}/pos_order_{0}_eDistance={1}_eStudents={2}_{4}{3}'.format(distance, distance_exponent, students_exponent, year, 'only_under_' if only_under else '', 'only_under' if only_under else 'not_only_under'))
    except IOError:
        if len(score_matrix) == 0:
            score_matrix = get_score(distance_exponent, students_exponent, year, distance, only_under, all_e = True)
        pos_order = [[i[0] for i in sorted(enumerate(score_student), key=lambda x:x[1], reverse=True)] for score_student in score_matrix]

        if all_elements:
            if year != 0:
                store_to_file(pos_order, '{0}/{5}/eDistance={1}/eStudents={2}/pos_order_{0}_eDistance={1}_eStudents={2}_{4}{3}'.format(distance, distance_exponent, students_exponent, year, 'only_under_' if only_under else '', 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(pos_order, '{0}/{4}/eDistance={1}/eStudents={2}/pos_order_{0}_eDistance={1}_eStudents={2}{3}'.format(distance, distance_exponent, students_exponent, '_only_under' if only_under else '', 'only_under' if only_under else 'not_only_under'))

    return pos_order
