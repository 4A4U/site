#-*- coding : utf-8 -*-
from .utils import get_from_file, store_to_file
from db.models import StudentChoice

def get_studentsChoice(year=0, only_under=True):
    try:
        if year != 0:
            studentsChoice = get_from_file('studentsChoice{0}_{1}'.format('_only_under' if only_under else '', year))
        else:
            studentsChoice = get_from_file('studentsChoice{0}'.format('_only_under' if only_under else ''))
    except IOError:
        if year != 0:
            if only_under:
                studentsChoice = StudentChoice.objects.filter(year__lt=year)
            else:
                studentsChoice = StudentChoice.objects.exclude(year=year)
            store_to_file(studentsChoice, 'studentsChoice{0}_{1}'.format('_only_under' if only_under else '', year))
        else:
            studentsChoice = StudentChoice.objects.all()
            store_to_file(studentsChoice, 'studentsChoice{0}'.format('_only_under' if only_under else ''))

    return studentsChoice
