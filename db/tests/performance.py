#-*- coding : utf-8 -*-
from .utils import get_from_file, store_to_file
from itertools import zip_longest

def get_performance(rank, e_distance = 0, e_students = 0, distance = 'jaccard', year = 0, only_under = True, position_matrix = [], all_e = False):
    try:
        if len(position_matrix) != 0:
            custom = not all_e
            if custom:
                raise IOError()
        custom = False

        if year == 0:
            performance = get_from_file('{2}/{5}/eDistance={0}/eStudents={1}/rank={4}/performance_{2}_rank={4}_eDistance={0}_eStudents={1}{3}'.format(e_distance, e_students, distance, '_only_under' if only_under else '', rank, 'only_under' if only_under else 'not_only_under'))
        else:
            performance = get_from_file('{2}/{6}/eDistance={0}/eStudents={1}/rank={5}/performance_{2}_rank={5}_eDistance={0}_eStudents={1}{3}_{4}'.format(e_distance, e_students, distance, '_only_under' if only_under else '', year, rank, 'only_under' if only_under else 'not_only_under'))

    except IOError:
        performance = [ [ len(list(filter(lambda x: x<i, filter(None, row))))/len(list(filter(None, row))) for i in range(rank)] for row in zip_longest(*position_matrix)]
        if not custom:
            if year == 0:
                store_to_file(performance, '{2}/{5}/eDistance={0}/eStudents={1}/rank={4}/performance_{2}_rank={4}_eDistance={0}_eStudents={1}{3}'.format(e_distance, e_students, distance, '_only_under' if only_under else '', rank, 'only_under' if only_under else 'not_only_under'))
            else:
                store_to_file(performance, '{2}/{6}/eDistance={0}/eStudents={1}/rank={5}/performance_{2}_rank={5}_eDistance={0}_eStudents={1}{3}_{4}'.format(e_distance, e_students, distance, '_only_under' if only_under else '', year, rank, 'only_under' if only_under else 'not_only_under'))

    return performance
