from db.models import Student, Formation, StudentChoice
import pickle
from itertools import zip_longest
from statistics import mean
import numpy as np
import decimal
from ..tests import get_students, get_position, get_studentsChoice, get_distance, get_score, avg, drange, get_from_file, store_to_file, get_formations, get_formations_similarity, get_formation_total, get_pos_order, get_performance, get_performance_score, get_avg_pos_order

def extract(kw, element, r_else = []):
    return kw[element] if element in kw else r_else

def gen_get(functions, year = 0, only_under = True, e_distance = [], e_students = [], coeffs = [], ranks = [], distances = 'all', **kwargs):
    if distances == 'all':
        distances = ['jaccard', 'hamming']
    if functions == 'all':
        functions = ['students', 'formations', 'distance', 'score', 'position', 'performance', 'formations_similarity', 'formations_total', 'pos_order', 'avg_pos_order', 'performance_score']

    studs = ('students' in kwargs)
    dist_or_stud = (studs or 'distance_matrix' in kwargs)
    know_formations = ('formations' in kwargs)
    know_formations_similarity = ('formations_similarity' in kwargs)
    if ('formations' in functions) and not know_formations:
        kwargs['formations'] = get_formations()
        functions.remove('formations')

    if ('students' in functions) and not studs:
        functions.remove('students')
        kwargs['students'] = get_students(year, only_under)

    if ('studentsChoice' in functions) and not ('studentsChoice' in kwargs):
        functions.remove('studentsChoice')
        kwargs['studentsChoice'] = get_studentsChoice(year, only_under)

    if ('formations_similarity' in functions) and not ('formations_similarity' in kwargs):
        functions.remove('formations_similarity')
        kwargs['formations_similarity'] = get_formations_similarity(extract(kwargs, 'formations'), not know_formations)

    if ('formations_total' in functions) and ('formations_total' not in kwargs):
        functions.remove('formations_total')
        kwargs['formations_total'] = get_formation_total(extract(kwargs, 'formations_similarity'))

    for distance in distances:
        kwargs[distance] = {}
        if ('distance' in functions) and not ('distance_matrix' in kwargs):
            kwargs[distance]['distance'] = get_distance(distance, year, only_under, extract(kwargs, 'students'), (not studs))

        for distance_exponent in e_distance:
            kwargs[distance][distance_exponent] = {}
            for students_exponent in e_students:
                kwargs[distance][distance_exponent][students_exponent] = {}
                if ('score' in functions) and not ('score_matrix' in kwargs):
                    kwargs[distance][distance_exponent][students_exponent]['score'] = get_score(distance_exponent, students_exponent, year, distance, only_under, extract(kwargs, 'students'), extract(kwargs, 'formations'), extract(extract(kwargs, distance), 'distance', extract(kwargs, 'distance_matrix')), (not dist_or_stud))

                if ('pos_order' in functions) and not ('pos_order' in kwargs):
                    kwargs[distance][distance_exponent][students_exponent]['pos_order'] = get_pos_order(
                    extract(
                        extract(
                            extract(
                                extract(kwargs, distance),
                                distance_exponent),
                            students_exponent),
                        'score',
                        extract(kwargs, 'score_matrix')
                    ),
                    distance_exponent, students_exponent, year, distance, only_under, not ('score_matrix' in kwargs or dist_or_stud))

                if ('avg_pos_order' in functions) and not ('avg_pos_order' in kwargs):
                    kwargs[distance][distance_exponent][students_exponent]['avg_pos_order'] = get_avg_pos_order(
                    extract(
                        extract(
                            extract(
                                extract(kwargs, distance),
                                distance_exponent),
                            students_exponent),
                        'score',
                        extract(kwargs, 'score_matrix')
                    ),
                    distance_exponent, students_exponent, year, distance, only_under, not ('score_matrix' in kwargs or dist_or_stud))

                if ('position' in functions) and not ('position_matrix' in kwargs):
                    kwargs[distance][distance_exponent][students_exponent]['position'] = get_position(distance_exponent, students_exponent, distance = distance, year = year, only_under = only_under, students = extract(kwargs, 'students'), formations = extract(kwargs, 'formations'), score_matrix = extract(
                        extract(
                            extract(
                                extract(kwargs, distance),
                                distance_exponent),
                            students_exponent),
                        'score',
                        extract(kwargs, 'score_matrix')
                    ),
                    all_e = not (dist_or_stud or know_formations))

                for rank in ranks:
                    kwargs[distance][distance_exponent][students_exponent][rank] = {}
                    if ('performance' in functions) and not ('performance' in kwargs):
                        kwargs[distance][distance_exponent][students_exponent][rank]['performance'] = get_performance(rank, distance_exponent, students_exponent, distance, year, only_under, extract(
                                extract(
                                    extract(
                                        extract(kwargs, distance),
                                        distance_exponent
                                    ),
                                    students_exponent
                            ),
                            'position',
                            extract(kwargs, 'position_matrix')
                        ), not ('position_matrix' in kwargs or dist_or_stud or know_formations))

                    if ('performance_score' in functions) and not ('performance_score' in kwargs):
                        kwargs[distance][distance_exponent][students_exponent][rank]['performance_score'] = {}
                        for coeff in coeffs:
                            kwargs[distance][distance_exponent][students_exponent][rank]['performance_score'][coeff] = get_performance_score(coeff, rank, distance_exponent, students_exponent, distance, year, only_under, extract(
                                extract(
                                    extract(
                                        extract(
                                            extract(kwargs, distance),
                                            distance_exponent
                                        ),
                                        students_exponent
                                    ),
                                    rank
                                ),
                                'performance',
                                extract(kwargs, 'performance')
                            ), not ('performance' in kwargs or 'position_matrix' in kwargs or dist_or_stud or know_formations))

    return kwargs
