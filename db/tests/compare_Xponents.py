#-*- coding : utf-8 -*-
from ..tests import get_from_file, store_to_file, drange, get_performance_score, get_performance

def compare_Xponents():
    compare_all = [[]]
    for rank in range(10,50,5):
        for coef in drange(0,1,0.2):
            try:
                compare = get_from_file('compare_rank={0}_coef={1}.txt'.format(rank,coef))
            except IOError:
                compare = []
                for e_distance in range(0, 10):
                    for e_students in drange(0, 1, 0.05):
                        compare.append(['{0}_{1}'.format(e_distance, e_students),
                            round(get_performance_score(
                                get_performance([[]], 0, e_distance=e_distance, e_students=e_students), coef, rank), 3) ])
                            #print ('{0}_{1}'.format(e_distance, e_students))
                compare = sorted(compare, key = lambda x: x[1], reverse= True)
                store_to_file(compare, 'compare_rank={0}_coef={1}.txt'.format(rank,coef))
            compare_all.append(['rank={0}_coef={1}'.format(rank,coef), [i[0] for i in compare[:3]]])
    for out in compare_all:
        print(out)
    store_to_file(compare_all, 'compare_all')
    return
