#-*- coding : utf-8 -*-
from .utils import get_from_file, store_to_file
from db.models import Formation

def get_formations():
    try:
        formations = get_from_file('formations')
    except IOError:
        formations = Formation.objects.all()
        store_to_file(formations, 'formations')

    return formations
