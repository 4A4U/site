# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-11 21:23
from __future__ import unicode_literals

import db.models
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='courses',
            field=db.models.VarBinaryField(max_length=800),
        ),
    ]
