from django.conf.urls import include, url
from rest_framework import routers

from django.contrib import admin
admin.autodiscover()

from .views import StudentViewSet, StudentChoiceViewSet, FormationViewSet, SchoolViewSet

router = routers.DefaultRouter()

router.register('students', StudentViewSet, base_name="students")
router.register('studentchoices', StudentChoiceViewSet, base_name="studentchoices")
router.register('formations', FormationViewSet, base_name="formations")
router.register('schools', SchoolViewSet, base_name="schools")

urlpatterns = [
    url(r'^', include(router.urls)),
]
