# -*- coding: utf-8 -*-
from rest_framework.viewsets import ModelViewSet
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework.filters import BaseFilterBackend
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_200_OK, HTTP_404_NOT_FOUND, HTTP_403_FORBIDDEN

from .models import Student, School, StudentChoice, Formation, Country
from .serializers import StudentSerializer, SchoolSerializer, StudentChoiceSerializer, FormationSerializer

from .utils import parse_PA, parse_pays, parse_categories

class StudentFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        PA_chosen = parse_PA('PA', request)
        return queryset.filter(pa__in=PA_chosen)

class StudentViewSet(ModelViewSet):
    queryset = Student.objects.all()
    serializer_class = StudentSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    filter_backends = (StudentFilterBackend, )

    def create(self, request):
        return Response("", status=HTTP_403_FORBIDDEN)

class StudentChoiceFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        PA_chosen = parse_PA('PA', request)
        typ = parse_categories('cats', request)
        countries = parse_pays('lieu', request)
        return queryset.filter(student__pa__in=PA_chosen, formation__type__in=typ, formation__school__country__in=countries)

class StudentChoiceViewSet(ModelViewSet):
    queryset = StudentChoice.objects.all()
    serializer_class = StudentChoiceSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    filter_backends = (StudentChoiceFilterBackend, )

    def create(self, request):
        return Response("", status=HTTP_403_FORBIDDEN)

class FormationFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        PA_chosen = parse_PA('PA', request)
        typ = parse_categories('cats', request)
        countries = parse_pays('lieu', request)
        return queryset.filter(studentchoices__student__pa__in=PA_chosen, type__in=typ, school__country__in=countries)

class FormationViewSet(ModelViewSet):
    queryset = Formation.objects.all()
    serializer_class = FormationSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    filter_backends = (FormationFilterBackend, )

    def create(self, request):
        return Response("", status=HTTP_403_FORBIDDEN)

class SchoolFilterBackend(BaseFilterBackend):
    def filter_queryset(self, request, queryset, view):
        PA_chosen = parse_PA('PA', request)
        typ = parse_categories('categorie', request)
        countries = parse_pays('lieu', request)
        return queryset.filter(formations__studentchoices__student__pa__in=PA_chosen, formations__type__in=typ, country__in=countries).distinct()


class SchoolViewSet(ModelViewSet):
    queryset = School.objects.all()
    serializer_class = SchoolSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, )
    filter_backends = (SchoolFilterBackend, )

    def create(self, request):
        return Response("", status=HTTP_403_FORBIDDEN)
