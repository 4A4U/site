# -*- coding: utf-8 -*-
from django.db.models import Count, Prefetch, Value

from rest_framework.serializers import ModelSerializer, SerializerMethodField

from .models import Student, School, StudentChoice, Formation
from .utils import parse_PA, parse_pays, parse_categories

class StudentSerializer(ModelSerializer):
    class Meta:
        model = Student
        exclude = ['courses', 'r_courses']

class StudentChoiceSerializer(ModelSerializer):
    class Meta:
        model = StudentChoice
        fields = '__all__'

    student = StudentSerializer()

class FormationSerializer(ModelSerializer):
    class Meta:
        model = Formation
        fields = '__all__'

    studentchoices = StudentChoiceSerializer(many=True)

class SchoolSerializer(ModelSerializer):
    class Meta:
        model = School
        fields = '__all__'

    numberStudents = SerializerMethodField()
    listOtherUnis = SerializerMethodField()

    def get_numberStudents(self, school):
        return Student.objects.filter(
            studentchoices__formation__university=school, 
            pa__in=parse_PA('PA', self.context['request']), 
            studentchoices__formation__university__country__in=parse_pays('lieu', self.context['request']),
            studentchoices__formation__type__in=parse_categories('categorie', self.context['request']),
            ).count()

    def get_listOtherUnis(self, school):
        return Student.objects.filter(pk__in=Student.objects.filter(
                studentchoices__formation__university=school, 
                pa__in=parse_PA('PA', self.context['request']), 
                studentchoices__formation__university__country__in=parse_pays('lieu', self.context['request']),
                studentchoices__formation__type__in=parse_categories('categorie', self.context['request']),
                )
            ).values('studentchoices__formation__university', 'pa').distinct()
