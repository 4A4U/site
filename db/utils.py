from .models import Country, Student

def parse_pays(name, request):
    pays = list(map(int, request.GET.getlist(name, [])))
    pays_empty = len(pays) == 0
    countries = Country.objects.all()
    if pays_empty:
        pays = countries
    return pays

def parse_categories(name, request):
    cats = request.GET.getlist(name, [])
    cats_empty = len(cats) == 0
    typ = []
    inter = 'Formation à l\'International'
    if cats_empty and len(cats) < 6:
        if 'corps' in cats:
            typ.append('Corps d\'Etat')
        """ à décocher si on arrive à différentier les catégories
        if 'cat1' in cats:
            typ.append(inter)
        if 'cat2' in cats:
            if inter not in typ:
                typ.append(inter)
        if 'cat3' in cats:
            pays.extend(['ETATS-UNIS', 'JAPON', 'ROYAUME-UNI', 'CANADA', 'AUSTRALIE'])
            if inter not in typ:
                typ.append(inter) """
        if 'inter' in cats:
            typ.extend([inter, 'EV2 en double diplôme - Formation à l\'internationa...'])
        if 'ecole' in cats:
            typ.append('Écoles')
        if 'master' in cats:
            typ.extend(['Master Catalogue Hors X', 'Master-EP', 'Master Paris-Saclay', 'Master hors X'])
        if 'hc' in cats:
            typ.append('Hors Catalogue')
        if 'phd' in cats:
            typ.append('Phd-Doctorat')
    if len(typ) == 0:
        typ = ['Corps d\'Etat', 'EV2 en double diplôme - Formation à l\'internationa...', inter, 'Écoles', 'Master Catalogue Hors X', 'Master-EP', 'Master Paris-Saclay', 'Master hors X', 'Hors Catalogue', 'Phd-Doctorat']
    return typ

def parse_PA(name, request):
    PA_chosen = request.GET.getlist(name, [])
    PA = Student.SUBJECTS
    if len(PA_chosen) == 0:
        PA_chosen = [pa[0] for pa in PA]
    return PA_chosen
