#-*- coding : utf-8 -*-
from django.db.models import Model, BooleanField, CharField, FloatField, TextField, IntegerField, ForeignKey, Field, Count, ManyToManyField
from scipy.spatial.distance import jaccard, hamming

class Country(Model):
    name = CharField(max_length=100)

class Language(Model):
    name = CharField(max_length=100)

class School(Model):
    CATEGORIES = (
            (0, ''),
            (1, 'Catégorie 1'),
            (2, 'Catégorie 2'),
            (3, 'Catégorie 3'),
            )
    name = CharField(max_length=100)
    category = IntegerField(choices=CATEGORIES)
    country = ForeignKey(Country, related_name="schools")
    lat = FloatField(default=None)
    lng = FloatField(default=None)

class FindSchool(Model):
    name = CharField(max_length=100)
    school = ForeignKey(School)

class Course(Model):
    YEARS = (
            (1, '1A'),
            (2, '2A'),
            (3, '3A'),
            )
    SUBJECTS = (
            ('MAT', 'Mathématiques'),
            ('MAP', 'Mathématiques appliquées'),
            ('BIO', 'Biologie'),
            ('INF', 'Informatique'),
            ('PHY', 'Physique'),
            ('MEC', 'Mécanique'),
            ('ECO', 'Économie'),
            ('CHI', 'Chimie'),
            )
    subject = CharField(choices=SUBJECTS, max_length=3)
    year = IntegerField(choices=YEARS)
    name = CharField(unique=True, max_length=7)

class VarBinaryField(Field):
    def to_python(self, value):
        """ DB -> Python """
        return int(value, 2)

    def from_db_value(self, value, expression, connection, context):
        return int(value, 2)

    def get_prep_value(self, value):
        """ Python -> DB """
        if isinstance(value, int):
            return bin(value)
        else:
            if isinstance(value, str):
                return value
            else:
                return None

    def db_type(self, connection):
        return 'varbinary({0})'.format(self.max_length)


class Student(Model):
    name = CharField(max_length=45)#, unique=True
    courses = VarBinaryField(max_length=1200)
    SUBJECTS = (
            ('MAT', 'Mathématiques'),
            ('MAP', 'Mathématiques appliquées'),
            ('BIO', 'Biologie'),
            ('INF', 'Informatique'),
            ('PHY', 'Physique'),
            ('MEC', 'Mécanique'),
            ('ECO', 'Économie'),
            ('CHI', 'Chimie'),
            )
    pa = CharField(choices=SUBJECTS, max_length=3)
    thematic = TextField()
    year = IntegerField()
    r_courses = ManyToManyField(Course, related_name="students")

    def sort_formation(self, distance='jaccard',exp_dist=9,exp_stud=0.15,only_under=True):
        from db.tests import get_score
        #liste des index des formations triés selon le score choisi
#        pos = get_pos_order(distance_exponent = exp_dist, students_exponent = exp_stud, year = 0, distance = distance, only_under = only_under)
        score = get_score(e_distance = exp_dist, e_students = exp_stud, year = 0, distance = distance, only_under = only_under)
#        #liste des positions des formations pour l'élève considéré
#        formation_position = [i[0] for i in sorted(enumerate(pos[self.id-1]), key=lambda x:x[1])]
        return score[self.id-1]

    def get_array_courses(self):
        return [digit == '1' for digit in list('{0:0800b}'.format(self.courses))]

    def get_array_courses_new(self):
        return [studentcurriculum for studentcurriculum in self.r_courses.all()]

    def diff_courses(self, student):
        return bin(self.courses ^ student.courses).count("1")/800

    def similarity_courses_new(self, student):
        courses1 = self.get_array_courses_new()
        courses2 = student.get_array_courses_new()
        d = len([element for element in courses1 if element in courses2])
        return d/(len(courses1) + len(courses2) - d)

    def similarity_courses(self, student):
        return bin(self.courses & student.courses).count("1") / ( bin(self.courses).count('1') + bin(student.courses).count('1')  - bin(self.courses & student.courses).count("1"))

    def diff_courses_new(self, student):
        return len(list(set(self.get_array_courses_new())^set(student.get_array_courses_new())))/len(Course.objects.all())

    def diff_courses_jaccard(self, student):
        return jaccard(self.get_array_courses(), student.get_array_courses())

    def diff_courses_jaccard_new(self, student):
        a = self.diff_courses_new(student)
        return a / (a + self.similarity_courses_new(student))

    def diff_courses_hamming(self, student):
        return hamming(self.get_array_courses(), student.get_array_courses())

    def diff_courses_hamming_new(self, student):
        return self.diff_courses_new(student) / Course.objects.all().annotate(nb=Count('pk')).values('nb')

    def diff_subjects(self, student2):
        subjects1 = Course.objects.filter(student=self).values('subject').annotate(total=Count('subject'))
        subjects2 = Course.objects.filter(student=student2).values('subject').annotate(total=Count('subject'))
        similarity = 0
        for subject in subjects1:
            similarity += min(subject['total'], subjects2.get(subject=subject['course__subject'])['total'])
        jaccardIndexSubject= similarity / ( sum(a['total'] for a in subjects1) + sum(a['total'] for a in subjects2) - similarity)
        return jaccardIndexSubject

    def error(self):
        error = 0
        for choice in self.studentchoices.filter(student=self.id):
            error += choice.ratio / choice.formation.score_ratio(self)
        return error

    def error_position(self):
        error = 0
        formations_ratio = sorted(Formation.objects.all(), key=lambda a: a.score_jaccard_ratio(self))
        for choice in  self.studentchoices.filter(student=self.id):
            error += choice.ratio / formations_ratio.index(choice.formation)
        return error

    def prediction_position(self):
        formations_ratio = sorted(Formation.objects.all(), key=lambda a: a.score_jaccard_ratio(self), reverse=True)
        choices =  self.studentchoices.filter(student=self.id)
        choice_position = [0 for i in range(len(choices))]
        for choice in choices:
            choice_position[choice.order -1]=formations_ratio.index(choice.formation)
        return choice_position


class Formation(Model):
    university = ForeignKey(School, related_name="formations")
    name = CharField(max_length=150)
    type = CharField(max_length=100)
    degree = CharField(max_length=100)
    duration = IntegerField()
    language = ForeignKey(Language)
    catalog = BooleanField(default=True)

    def score(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += student.similarity_courses(studentchoice.student) ** 2
        s /= len(self.studentchoices.all())
        return s

    def score_ratio(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += studentchoice.ratio * student.similarity_courses(studentchoice.student) ** 2
        s /= len(self.studentchoices.all())
        return s

    def score_hamming(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += student.diff_courses_hamming(studentchoice.student)
        s /= len(self.studentchoices.all())
        return s

    def score_hamming_ratio(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += studentchoice.ratio * student.diff_courses_hamming(studentchoice.student)
        s /= len(self.studentchoices.all())
        return s

    def score_jaccard(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += student.diff_courses_jaccard(studentchoice.student) ** 0.9
        s /= len(self.studentchoices.all()) ** 0.15
        return s

    def score_jaccard_ratio(self, student, only_under=False):
        s = 0
        if (len(self.studentchoices.all()) == 0):
            print("ERROR : formation non choisie : {0}".format(self.id))
            return s
        if only_under:
            studentchoices = self.studentchoices.exclude(student__year__gte=student.year)
        else:
            studentchoices = self.studentchoices.exclude(student__year=student.year)
        for studentchoice in studentchoices:
            s += studentchoice.ratio * student.diff_courses_jaccard(studentchoice.student) ** 0.9
        s /= len(self.studentchoices.all()) ** 0.15
        return s

class MinMaxFloatField(FloatField):
    def __init__(self, min_value=None, max_value=None, *args, **kwargs):
        self.min_value, self.max_value = min_value, max_value
        super(MinMaxFloatField, self).__init__(*args, **kwargs)

    def formfield(self, **kwargs):
        defaults = {'min_value': self.min_value, 'max_value' : self.max_value}
        defaults.update(kwargs)
        return super(MinMaxFloatField, self).formfield(**defaults)

class StudentChoice(Model):
    STATUS = (
            (1, 'admis'),
            (2, 'refusé'),
            (3, 'abandon'),
            (0, 'en attente'),
            )
    student = ForeignKey(Student, related_name="studentchoices")
    formation = ForeignKey(Formation, related_name="studentchoices")
    order = IntegerField()
    status = IntegerField(choices=STATUS)
    ratio = MinMaxFloatField(min_value=0.0, max_value=1.0)

class StudentSatisfaction(Model):
    country = ForeignKey(Country)
    university = ForeignKey(School)
    duration = FloatField(default=0)
    student = ForeignKey(Student)
    faculty = CharField(max_length=200)
    degree_type = CharField(max_length=100)
    degree_name = CharField(max_length=100)

class Question(Model):
    question = CharField(max_length=200)

class StudentAnswer(Model):
    ANSWERS = (
        (1, "Entièrement en désaccord"),
        (2, "Plutôt en désaccord"),
        (3, "Plutôt d'accord"),
        (4, "Entièrement d'accord")
    )
    question = ForeignKey(Question)
    formation = ForeignKey(StudentSatisfaction)
    answer = IntegerField(default=0, choices=ANSWERS)

class CurrentStudent(Model):
    name = CharField(max_length=45, unique=True)
    year = IntegerField()
    courses = ManyToManyField(Course, related_name="current_student")
    courseVector = VarBinaryField(max_length=1200)

    def get_array_courses(self):
        return [digit == '1' for digit in list('{0:0800b}'.format(self.courseVector))]

    def get_array_courses_new(self):
        return [studentcurriculum for studentcurriculum in self.courses.all()]

    def diff_courses(self, student):
        return bin(self.courseVector ^ student.courses).count("1")/800

    def similarity_courses_new(self, student):
        courses1 = self.get_array_courses_new()
        courses2 = student.get_array_courses_new()
        d = len([element for element in courses1 if element in courses2])
        return d/(len(courses1) + len(courses2) - d)

    def similarity_courses(self, student):
        return bin(self.courseVector & student.courses).count("1") / ( bin(self.courseVector).count('1') + bin(student.courses).count('1')  - bin(self.courseVector & student.courses).count("1"))

    def diff_courses_new(self, student):
        return len(list(set(self.get_array_courses_new())^set(student.get_array_courses_new())))/len(Course.objects.all())

    def diff_courses_jaccard(self, student):
        return jaccard(self.get_array_courses(), student.get_array_courses())

    def diff_courses_jaccard_new(self, student):
        a = self.diff_courses_new(student)
        return a / (a + self.similarity_courses_new(student))

    def diff_courses_hamming(self, student):
        return hamming(self.get_array_courses(), student.get_array_courses())

    def diff_courses_hamming_new(self, student):
        return self.diff_courses_new(student) / Course.objects.all().annotate(nb=Count('pk')).values('nb')
