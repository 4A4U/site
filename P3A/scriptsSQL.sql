// From listCourse to Course
SELECT *, SUBSTRING(`COL 1`, 1, 3),SUBSTRING(`COL 1`, 4 , 1) FROM `app_list_course_sci_noyear`


//from DSI to country
Select Distinct `PAYS_LIB`

//from DSI to School
Select Distinct `ETABLISSEMENT`, `CAT`, country.id, FROM `dsi` join country on country.name = dsi.pays_lib;

//from DSI to formation
 insert INTO db_formation (name, type , degree,  university_id)
select formation, type_formation, type_diplome, db_school.id from dsi join db_school on db_school.name=dsi.ETABLISSEMENT
group by etablissement, formation


INSERT INTO `db_studentchoice` (`order`, `status`, `formation_id`, `student_id`)
select `ORDRE DE CHOIX`, AVIS_JURY, db_formation.id, substring(ANONYMAT_PROJET,6,4)
from dsi
JOIN db_school on db_school.name=dsi.ETABLISSEMENT
join db_formation  on db_formation.name=dsi.FORMATION AND db_school.id=db_formation.university_id

UPDATE `db_studentchoice`
JOIN (SELECT student_id, count(*) as cnt FROM `db_studentchoice`  GROUP BY student_id) as grp
ON db_studentchoice.student_id = grp.student_id
SET ratio =  (grp.count + 1 - db_studentchoice.order) * 2 / (grp.count * (grp.count+1))



// Engineering of pa features
UPDATE `db_student`
SET `pa` = CASE
           WHEN `pa`='' THEN 'NPA'
           WHEN `pa`='Hor' THEN 'NPA'
           WHEN `pa`='PA ' THEN 'NPA'
           WHEN `pa`='Ele' THEN 'PHY'
           WHEN `pa`='Ene' THEN 'PHY'
           WHEN `pa`='Sci' THEN 'PHY'
           WHEN `pa`='Inn' THEN 'MIE'
           ELSE `pa`
        END
WHERE `pa` in ('', 'Hor', 'PA ', 'Ele', 'Ene', 'Inn', 'Sci')

UPDATE `db_student` SET `pa` = UPPER( `pa` )

#from csv to real_tables
INSERT INTO db_studentsatisfaction (duration, faculty, degree_type, degree_name, country_id, student_id, university_id)
SELECT `Duree`, `Faculte de rattachement`, `Type de diplome`, `Intitule diplome`, c.id AS country_id, substring(ANONYMAT,6,4) AS student_id, fs.school_id AS university_id
FROM `TABLE 23`
JOIN db_country AS c
JOIN db_findschool AS fs
ON `TABLE 23`.`Etablissement` = fs.name AND c.name LIKE (REPLACE (`TABLE 23`.`Pays`, ' ', '%'))

INSERT INTO db_studentanswer (question_id, formation_id, answer)
SELECT "1", `id`, `Q1`
FROM `TABLE 23`
