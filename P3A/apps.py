from django.apps import AppConfig


class P3AConfig(AppConfig):
    name = 'P3A'
