# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class AppAllForeign(models.Model):
    anonymat = models.CharField(db_column='ANONYMAT', max_length=9, blank=True, null=True)  # Field name made lowercase.
    pa = models.CharField(db_column='PA', max_length=48, blank=True, null=True)  # Field name made lowercase.
    parcours = models.CharField(db_column='Parcours', max_length=100, blank=True, null=True)  # Field name made lowercase.
    pays = models.CharField(db_column='Pays', max_length=90, blank=True, null=True)  # Field name made lowercase.
    etablissement = models.CharField(db_column='Etablissement', max_length=74, blank=True, null=True)  # Field name made lowercase.
    faculte_de_rattachement = models.CharField(db_column='Faculte de rattachement', max_length=120, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type_de_diplome = models.CharField(db_column='Type de diplome', max_length=39, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intitule_diplome = models.CharField(db_column='Intitule diplome', max_length=95, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    duree = models.CharField(db_column='Duree', max_length=62, blank=True, null=True)  # Field name made lowercase.
    null = models.IntegerField(db_column='NULL', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'app_all_foreign'


class AppCourseSciNoyear(models.Model):
    anonymat = models.CharField(db_column='ANONYMAT', max_length=20)  # Field name made lowercase.
    coursevector = models.CharField(db_column='CourseVector', max_length=800)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'app_course_sci_noyear'


class AppListCourseSciNoyear(models.Model):
    col_1 = models.CharField(db_column='COL 1', max_length=14, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'app_list_course_sci_noyear'


class AppMapQuestion(models.Model):
    col_1 = models.CharField(db_column='COL 1', max_length=3, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    col_2 = models.CharField(db_column='COL 2', max_length=122, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.

    class Meta:
        managed = False
        db_table = 'app_map_question'


class AppQuestion(models.Model):
    anonymat = models.CharField(db_column='ANONYMAT', max_length=9, blank=True, null=True)  # Field name made lowercase.
    pa = models.CharField(db_column='PA', max_length=50, blank=True, null=True)  # Field name made lowercase.
    parcours = models.CharField(db_column='Parcours', max_length=151, blank=True, null=True)  # Field name made lowercase.
    pays = models.CharField(db_column='Pays', max_length=11, blank=True, null=True)  # Field name made lowercase.
    etablissement = models.CharField(db_column='Etablissement', max_length=74, blank=True, null=True)  # Field name made lowercase.
    faculte_de_rattachement = models.CharField(db_column='Faculte de rattachement', max_length=92, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    type_de_diplome = models.CharField(db_column='Type de diplome', max_length=33, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    intitule_diplome = models.CharField(db_column='Intitule diplome', max_length=95, blank=True, null=True)  # Field name made lowercase. Field renamed to remove unsuitable characters.
    duree = models.CharField(db_column='Duree', max_length=4, blank=True, null=True)  # Field name made lowercase.
    q1 = models.IntegerField(db_column='Q1', blank=True, null=True)  # Field name made lowercase.
    q2 = models.IntegerField(db_column='Q2', blank=True, null=True)  # Field name made lowercase.
    q3 = models.IntegerField(db_column='Q3', blank=True, null=True)  # Field name made lowercase.
    q4 = models.IntegerField(db_column='Q4', blank=True, null=True)  # Field name made lowercase.
    q5 = models.IntegerField(db_column='Q5', blank=True, null=True)  # Field name made lowercase.
    q6 = models.IntegerField(db_column='Q6', blank=True, null=True)  # Field name made lowercase.
    q7 = models.IntegerField(db_column='Q7', blank=True, null=True)  # Field name made lowercase.
    q8 = models.IntegerField(db_column='Q8', blank=True, null=True)  # Field name made lowercase.
    q9 = models.IntegerField(db_column='Q9', blank=True, null=True)  # Field name made lowercase.
    q10 = models.IntegerField(db_column='Q10', blank=True, null=True)  # Field name made lowercase.
    q11 = models.IntegerField(db_column='Q11', blank=True, null=True)  # Field name made lowercase.
    q12 = models.IntegerField(db_column='Q12', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'app_question'
