#!/bin/bash

for distance in 'hamming' 'jaccard'
do
    `mkdir $distance`
    `mv *$distance* $distance/`
    cd $distance/
    for year in 'only_under' 'not_only_under'
    do
        `mkdir $year`
        if [ $year = 'not_only_under' ]
        then
            echo "not_only_under"
            `mv *.txt $year/`
        else
            `mv *$year* $year/`
        fi
        cd $year/
        for d in `seq 0 19`
        do
            `mkdir eDistance=$d`
            `mv *eDistance=$d\_* eDistance=$d/`
            cd eDistance=$d/
            for s in $(echo `seq 0 0.05 1` | sed 's/,00//g' | sed -e 's/,/./g' | sed -e 's/\([1-9]\)0/\1/g')
            do
                `mkdir eStudents=$s`
                if [ $year = 'not_only_under' ]
                then
                    `mv *eStudents=$s.txt eStudents=$s/`
                else
                    `mv *eStudents=$s\_* eStudents=$s/`
                fi
                cd eStudents=$s/
                for rank in `seq 10 10 50`
                do
                    `mkdir rank=$rank`
                    `mv *rank=$rank* rank=$rank/`
                    cd rank=$rank/
                    for c in $(echo `seq 0.5 0.1 1` | sed 's/,00//g' | sed -e 's/,/./g' | sed -e 's/\([1-9]\)0/\1/g')
                    do
                        `mkdir coeff=$c`
                        `mv *coeff=$c\_* coeff=$c/`
                    done
                    cd ../
                done
                cd ../
            done
            cd ../
        done
        cd ../
    done
    cd ../
done
