#! /bin/bash

name=`expr $TAR_ARCHIVE : '[./]\{0,2\}\([a-z]*\)[0-9]*\.tar\.gz'`
n=${name:-$TAR_ARCHIVE}
echo $n
echo Preparing volume $TAR_VOLUME of archive $n
echo $n$TAR_VOLUME.tar.gz >&$TAR_FD
