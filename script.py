#-*- coding : utf-8 -*-
import csv
import datetime
from db.models import Student, Formation, Course, CurrentStudent, School
from key import API_KEY

def create_matrix():
    students = Student.objects.all()
    # create distance matrix
    matrix = [[student1.diff_courses(student2) for student1 in students] for student2 in students]
    print('\n'.join([''.join(['{:4}'.format(item) for item in row]) for row in matrix]))

def test_formation():
    student = Student.objects.get(pk=1)
    formations = Formation.objects.all()
    for formation in formations:
        print(formation.score(student))

# TODO : test, add debug ?
def import_current_students():
    """
        Function to import the current students of the Ecole polytechnique :
            - add new courses to the list of existent courses, without duplicate
            - add new students to the list of current students, without duplicate
            - foreach student, register the list of his/her courses, without duplicate
    """
    #import lines of csv file
    data = csv.reader(open('current.csv'),delimiter=';')
    #ALTER TABLE db_currentstudent AUTO_INCREMENT=0
    #get list of (id, course_name) for the header of the csv file
    sci_subjects = ["BIO","CHI","INF","ECO","MAP","MAT","MEC","PHY"]
    #courses = [Course.objects.get_or_create(name=course_name, subject=course_name[:3], year=(int(course_name[4])+1)).id for course_name in header if course_name != ""]#what to do if problems ?
    #for each line
    counter_import = 0
    for line in data:
        #Some course have the format MAT/PHY575 we chose to keep the last one
        counter_import +=1
        print(str(counter_import)+" lines imported")
        courses = [x.split('/')[-1:][0] for x in line[3].split(',')]
        #print(courses)
        sci_courses = [course for course in courses if course[:3] in sci_subjects]
        #print(sci_courses)
        #print(sci_courses[0][3])

        db_courses = [Course.objects.get_or_create(name=course_name, subject=course_name[:3], year=int(course_name[3]))[0] for course_name in sci_courses]
        #print(db_courses)
        db_courses_index= [x.id-1 for x in db_courses]
        #print(db_courses_index)
        course_vector = [0 for i in range(Course.objects.count())]
        course_vector = [1 if i in db_courses_index else 0 for i in range(Course.objects.count())]
        #print(course_vector)
        course_vector_string = "".join([str(x) for x in course_vector])
        #print(course_vector_string)
        #  for each column, if 1, insert into courses of the current student
        student = CurrentStudent.objects.get_or_create(name=line[0],defaults={'year': line[1], 'courseVector':course_vector_string})[0]
        student.courseVector = course_vector_string
        for course in db_courses:
            student.courses.add(course)
        student.year= line[1]
        student.save()

def import_former_students():
    """
        Function to import the former students of the Ecole polytechnique :
            - add new students to the list of former students, without duplicate
            - foreach new former student, register the list of his/her courses, without duplicate
    """
    #import lines of csv file
    data = csv.reader(open('files/former.csv'))
    #get list of (id, course_name) for the header of the csv file
    header = data.next()
    courses = [Course.objects.get(name=course_name).id for course_name in header if course_name != ""]#what to do if problems ?
    #for each line
    for line in data:
    #  get id of student/created if doesn't exist
        student = Students.objects.get_or_create(name=line[0], year=line[1], pa=line[2], thematic=line[3]) #how to know which year they belong to <- is it important ?
        #  for each column, if 1, insert into courses of the current student
        for i in range(len(line)-4):
            if line[i+4] == 1:
                student.r_courses.add(courses[i]) #add without duplicate
        student.save()

import requests

def save_from_distant():
    url = ''
    resp = requests.get(url)

def get_coordinates_university():
    universities = School.objects.filter(pk__gt=3, lat=0, lng=0)
    for university in universities:
        # request name, country
        print(university.id)
        resp = requests.get("https://maps.googleapis.com/maps/api/geocode/json?address={0}, {1}&key={2}".format(university.name, university.country.name, API_KEY))
        response = resp.json()["results"]
        if(len(response)==0):
            print("University not found")
        else:
            lat = response[0]["geometry"]["location"]["lat"]
            lng = response[0]["geometry"]["location"]["lng"]
            university.lat = lat
            university.lng = lng
            university.save()
