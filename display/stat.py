
from db.models import Student, Formation, StudentChoice
import pickle
from itertools import zip_longest
from statistics import mean
import numpy as np
import decimal


def test(opt1='',opt2='',opt3='', e_distance=0, e_students=0):
    print('Beginning test')

    students = get_students()
    studentsChoice = get_studentsChoice()
    formations = get_formations()
    distance_matrix = get_distance(students, opt1)
    score_matrix = get_score(students, formations, studentsChoice, distance_matrix, e_distance, e_students, opt2)
    position_matrix = get_position(students, formations, score_matrix, e_distance, e_students, opt3)
    performance = get_performance(position_matrix, len(formations),e_distance, e_students)
    average = map(avg, zip_longest(*position_matrix))

    #avg_pos_order = map(avg, zip(*[[i[0] for i in sorted(enumerate(score_student), key=lambda x:x[1],reverse=True)] for score_student in score_matrix]))
    #[np.percentile(np.array(list(filter(None,row))), np.arange(10,100,10)) for row in zip_longest(*position_matrix)]

    return performance

def compare_Xponents():
    compareAll=[[]]
    for rank in range(10,50,5):
        for coef in drange(0,1,0.2):
            try:
                with open('DataDump/compare_rank={0}_coef={1}.txt'.format(rank,coef), 'rb') as fp:
                    compare = pickle.load(fp)
                    print('compare_rank={0}_coef={1} imported'.format(rank,coef) )
            except IOError:
                compare =[]
                for e_distance in range(0, 10):
                    for e_students in drange(0, 1, 0.05):
                            compare.append(['{0}_{1}'.format(e_distance, e_students),
                                        round(get_performance_score(
                                            get_performance([[]],0,e_distance=e_distance, e_students=e_students),coef,rank),3) ])
                            # replace get_performance with test()  for the first computation
                            #print ('{0}_{1}'.format(e_distance, e_students))
                compare = sorted(compare, key = lambda x: x[1], reverse= True)
                with open('DataDump/compare_rank={0}_coef={1}.txt'.format(rank,coef), 'wb') as fp:
                    pickle.dump(compare, fp)
            compareAll.append(['rank={0}_coef={1}'.format(rank,coef) , [i[0] for i in compare[:3]]])
    for out in compareAll:
        print(out)
    with open('DataDump/compare_All.txt'.format(rank,coef), 'wb') as fp:
        pickle.dump(compareAll, fp)
    return


def get_students():
    try:
        with open('DataDump/students.txt', 'rb') as fp:
            students = pickle.load(fp)
        print('students imported')
    except IOError:
        students = Student.objects.all()
        with open('DataDump/students.txt', 'wb') as fp:
             pickle.dump(students, fp)
    print('Students loaded')
    return students

def get_studentsChoice():
    try:
        with open('DataDump/studentchoice.txt', 'rb') as fp:
            studentsChoice = pickle.load(fp)
        print('studentChoices imported')
    except IOError:
        studentsChoice = StudentChoice.objects.all()
        with open('DataDump/studentchoice.txt', 'wb') as fp:
            pickle.dump(studentsChoice, fp)
    print('choices loaded')
    return studentsChoice

def get_formations():
    try:
        with open('DataDump/formation.txt', 'rb') as fp:
            formations = pickle.load(fp)
            print('formations imported')
    except IOError:
        formations = Formation.objects.all()
        with open('DataDump/formation.txt', 'wb') as fp:
            pickle.dump(formations, fp)
    print('formation loaded')
    return formations

def get_distance(students, opt=''):
    try:
        with open('DataDump/distance'+opt+'.txt', 'rb') as fp:
            distance_matrix = pickle.load(fp)
        print('distance'+opt+' imported')
    except IOError:
        distance_matrix = [[bin(u.courses & v.courses).count("1") / ( bin(u.courses).count('1') + bin(u.courses).count('1')  - bin(u.courses & v.courses).count("1")) for u in students ] for v in students]
        with open('DataDump/distance'+opt+'.txt', 'wb') as fp:
            pickle.dump(distance_matrix, fp)
        print('distance'+opt+' computed and saved')
    return distance_matrix

def get_score(students, formations, studentsChoice, distance_matrix, e_distance=0, e_students=0, opt=''):
    try:
        with open('DataDump/score{0}_eDistance={1}_eStudents={2}.txt'.format(opt, e_distance, e_students), 'rb') as fp:
            score_matrix = pickle.load(fp)
        print('score_matrix'+opt+' imported')
    except IOError:
        score_matrix = [[0 for x in range(len(formations))] for y in range(len(students))]
        print('begin score computation')
        i=0
        for formation in formations:
            #print(i)
            #i+=1
            for studentchoice in formation.studentchoice_set.all():
                coeff = studentchoice.ratio / len(formation.studentchoice_set.all()) ** e_students
                for stud in students:
                    score_matrix[stud.id-1][formation.id-1] +=  coeff * (distance_matrix[stud.id-1][studentchoice.student.id-1] ** e_distance)
            ##[[sum([studentchoice.ratio/len(formation.studentchoice_set.all()) * distance_matrix[stud.id-1][studentchoice.student.id-1] for studentchoice in formation.studentchoice_set.all()]) for stud in students] for formation in formations]
        with open('DataDump/score{0}_eDistance={1}_eStudents={2}.txt'.format(opt, e_distance, e_students), 'wb') as fp:
            pickle.dump(score_matrix, fp)
        print('score {0} computed and saved for eDistance = {1} and eStudents = {2}'.format(opt, e_distance, e_students))
    return score_matrix

def get_position(students, formations, score_matrix, e_distance=0, e_students=0, opt=''):
    try:
        with open('DataDump/position{0}_eDistance={1}_eStudents={2}.txt'.format(opt, e_distance, e_students), 'rb') as fp:
            position_matrix = pickle.load(fp)
            print('position'+opt+' imported')
    except IOError:
        position_matrix  = [ [ sorted(formations, key=lambda a: score_matrix[stud.id-1][a.id-1],reverse=True).index(choice.formation) for choice in stud.studentchoice_set.order_by('order').all()] for stud in students]

        with open('DataDump/position{0}_eDistance={1}_eStudents={2}.txt'.format(opt, e_distance, e_students), 'wb') as fp:
            pickle.dump(position_matrix, fp)
        print('position {0} computed and saved for eDistance = {1} and eStudents = {2}'.format(opt, e_distance, e_students))
    return position_matrix

def get_student_score(student, opt2='_2'):
    try:
        with open('DataDump/score'+opt2+'.txt', 'rb') as fp:
            score_matrix = pickle.load(fp)
        print('score_matrix'+opt2+' imported')
    except IOError:
        score_matrix=compute_score(students, formations, studentsChoice, distance_matrix,opt2)
    return score_matrix[student.id-1]

def get_performance(position_matrix, length,e_distance, e_students):
    try:
        with open('DataDump/performance_eDistance={0}_eStudents={1}.txt'.format(e_distance, e_students), 'rb') as fp:
            performance = pickle.load(fp)
            print('performance imported')
    except IOError:
        performance = [ [ len(list(filter(lambda x: x<i,filter(None, row))))/len(list(filter(None, row))) for i in range(length)] for row in zip_longest(*position_matrix)]
        with open('DataDump/performance_eDistance={0}_eStudents={1}.txt'.format(e_distance, e_students), 'wb') as fp:
            pickle.dump(performance, fp)
    return performance

def get_pos_order(score_matrix):
    return [[i[0] for i in sorted(enumerate(score_student), key=lambda x:x[1],reverse=True)][:20] for score_student in score_matrix]

def get_avg_pos_order(score_matrix):
    return map(avg, zip(*[[i[0] for i in sorted(enumerate(score_student), key=lambda x:x[1],reverse=True)] for score_student in score_matrix]))

def get_performance_score(perf, coef=0.8, rank=30):
    score =0;
    for i in range(len(perf)):
        score += perf[i][rank] * (coef**i)
    return score

def get_formations_similarity():
    try:
        with open('DataDump/formationSimilarity', 'rb') as fp:
            formationSimilarity = pickle.load(fp)
            print('formationSimilarity imported')
    except IOError:
        formationLength = len(Formation.objects.all())
        students = get_students()
        studentsChoice = StudentChoice.objects.all()
        choiceByStudent = [[] for f in range(len(students))]
        for choice in studentsChoice:
            choiceByStudent[choice.student_id-1].append(choice.formation_id)
        formationSimilarity = [[0 for i in range(formationLength)] for j in range(formationLength)]
        for choices in choiceByStudent:
            for formation1 in choices:
                for formation2 in choices :
                    formationSimilarity[formation1-1][formation2-1]+=1
        with open('DataDump/formationSimilarity', 'wb') as fp:
                pickle.dump(formationSimilarity, fp)
    return formationSimilarity

def get_formation_total():
    formationSimilarity = get_formations_similarity()
    return [formationSimilarity[i][i] for i in range(formationSimilarity)]

def avg(x):
    x = filter(None, x)
    return  mean(x)


def drange(x, y, jump):
    while x < y:
        yield round(float(x),2)
        x += decimal.Decimal(jump)

