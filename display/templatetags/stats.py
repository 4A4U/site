from django import template

register = template.Library()

@register.filter(name='PA')
def PA(university, pa):
    return getattr(university, "students_{0}".format(pa))
