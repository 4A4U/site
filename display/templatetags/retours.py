from django import template
from db.models import StudentAnswer

register = template.Library()

@register.filter(name='response')
def response(answer):
    if answer == '':
        return
    return StudentAnswer.ANSWERS[int(answer) - 1][1];
