from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.fake_login, name='fake_login'),
    url(r'^test$', views.test, name='test'),
    url(r'^home/$', views.home, name='home'),
    url(r'^home/(?P<student_id>[0-9]+)/$', views.home, name='home'),
    url(r'^conseils_4A/$', views.conseils_4A, name='conseils_4A'),
    url(r'^conseils_4A/(?P<student_id>[0-9]+)/$', views.conseils_4A, name='conseils_4A'),
    url(r'^conseils_4A/(?P<nb_results>[0-9]+)/$', views.conseils_4A, name='conseils_4A'),
    url(r'^conseils_4A/(?P<student_id>[0-9]+)/(?P<nb_results>[0-9]+)/$', views.conseils_4A, name='conseils_4A'),
    url(r'^login$', views.fake_login, name='fake_login'),
    url(r'^test2$', views.test2, name='test2'),
    url(r'^testModel$', views.testModel, name='testModel'),
    url(r'^retours_4A/$', views.retours_4A, name='retours_4A'),
    url(r'^retours_4A/(?P<student_id>[0-9]+)/$', views.retours_4A, name='retours_4A'),
    url(r'^stats/$', views.nb_students_per_school, name='stats'),
    url(r'^stats/(?P<student_id>[0-9]+)/$', views.nb_students_per_school, name='stats'),
    url(r'^map$', views.mapView, name='map'),
    url(r'^map/(?P<student_id>[0-9]+)/$', views.mapView, name='map'),
    url(r'^schools$', views.schools, name='schools'),
    url(r'^testNoCompute/$', views.testNoCompute, name='testNoCompute'),
    url(r'^testNoCompute/(?P<student_id>[0-9]+)/$', views.testNoCompute, name='testNoCompute'),
    url(r'^testNoCompute/(?P<nb_resultats>[0-9]+)/$', views.testNoCompute, name='testNoCompute'),
    url(r'^testNoCompute/(?P<student_id>[0-9]+)/(?P<nb_resultats>[0-9]+)/$', views.testNoCompute, name='testNoCompute'),
    url(r'^logout/', views.index, name="logout"),
]
