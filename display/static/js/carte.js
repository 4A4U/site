// initialize the map when the page is loaded
//google.maps.event.addDomListener( window, 'load', initialize);
var mapType = "map", adjust = true, zoneMarkers = new google.maps.LatLngBounds(), map, markers = [], generateIconCache = {};

var mapTypes = {
    map: google.maps.MapTypeId.ROADMAP,
    satellite: google.maps.MapTypeId.SATELLITE,
    earth: google.maps.MapTypeId.TERRAIN,
    hybrid: google.maps.MapTypeId.HYBRID
};
var travelModes = {
    driving: google.maps.DirectionsTravelMode.DRIVING,
    walking: google.maps.DirectionsTravelMode.WALKING,
    cycling: google.maps.DirectionsTravelMode.CYCLING
};

var markersIcons = [
    'http://maps.google.com/mapfiles/marker_yellow.png', 
    'http://maps.google.com/mapfiles/marker_green.png',
    'http://maps.google.com/mapfiles/marker_purple.png',
    'http://maps.google.com/mapfiles/marker.png',
    'http://maps.google.com/mapfiles/marker_orange.png',
    'http://maps.google.com/mapfiles/marker_white.png',
    'http://maps.google.com/mapfiles/marker_black.png',
    'http://maps.google.com/mapfiles/marker_grey.png',
    'http://maps.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png'
];

function generateIcon(number, callback) {
    if (generateIconCache[number] !== undefined) {
        callback(generateIconCache[number]);
    }
    var fontSize = 16, imageWidth = imageHeight = 35;
    if (number >= 1000) {
        fontSize = 10;
        imageWidth = imageHeight = 55;
    } 
    else if (number < 1000 && number > 100) {
        fontSize = 14;
        imageWidth = imageHeight = 45;
    }
    var svg = d3.select(document.createElement('div')).append('svg').attr('viewBox', '0 0 54.4 54.4').append('g')
    var circles = svg.append('circle').attr('cx', '27.2').attr('cy', '27.2').attr('r', '21.2').style('fill', '#2063C6');
    var path = svg.append('path').attr('d', 'M27.2,0C12.2,0,0,12.2,0,27.2s12.2,27.2,27.2,27.2s27.2-12.2,27.2-27.2S42.2,0,27.2,0z M6,27.2 C6,15.5,15.5,6,27.2,6s21.2,9.5,21.2,21.2c0,11.7-9.5,21.2-21.2,21.2S6,38.9,6,27.2z').attr('fill', '#FFFFFF');
    var text = svg.append('text').attr('dx', 27).attr('dy', 32).attr('text-anchor', 'middle').attr('style', 'font-size:' + fontSize + 'px; fill: #FFFFFF; font-family: Arial, Verdana; font-weight: bold').text(number);
    var svgNode = svg.node().parentNode.cloneNode(true), image = new Image();

    d3.select(svgNode).select('clippath').remove();

    var xmlSource = (new XMLSerializer()).serializeToString(svgNode);

    image.onload = (function(imageWidth, imageHeight) {
        var canvas = document.createElement('canvas'), context = canvas.getContext('2d'), dataURL;
        d3.select(canvas).attr('width', imageWidth).attr('height', imageHeight);
        context.drawImage(image, 0, 0, imageWidth, imageHeight);
        dataURL = canvas.toDataURL();
        generateIconCache[number] = dataURL;
        callback(dataURL);
    }).bind(this, imageWidth, imageHeight);

    image.src = 'data:image/svg+xml;base64,' + btoa(encodeURIComponent(xmlSource).replace(/%([0-9A-F]{2})/g, function(match, p1) {
        return String.fromCharCode('0x' + p1);
    }));
}

/** create and place a marker on a map
 * @int n: index our the created marker
 * @map map: map where the marker will be placed
 * @float lat, long: coordinates of the marker on the map
 * @string url: url of the marker's image
 * @int height, width: size of the marker
 * @int originX, originY: placement of the marker's image in the layout
 * @int anchorX, anchorY: placement of the marker relative to the coordinates
 * @int scaleX, @scaleY: scaled size of the marker's image
 * @boolean optimized: true if all the markers have to be united in one graphic (better for time complexity, worse for resolution)
 * @string title: message to show onmouseover
 * @string content: content of the InfoWindow displayed on click, if empty, no InfoWindow
 * @function action: function to call on click in addition to the InfoWindow
 * @int number : number to place in the icon
 * @function then : what to do at the end of the function
 */
function createMarker(n, map, lat, long, url = '', height = null, width = null, originX = null, originY = null, anchorX = null, anchorY = null, scaleHeight = null, scaleWidth = null, optimized = null, title = null, content = '', action, number = 0, then = function(){}) {

    /* if no url has been provided, pick up a random one in our list of register pointers */
    if(url == '') {
        var i = Math.floor(Math.random() * markersIcons.length);
        url = markersIcons[i];
    }
    var icon = {url: url}; // instanciate the icon
    /* if  the sizes and positions have been specified, use them */
    if(width !== null && height !== null) {
        icon.size = new google.maps.Size(width, height);
    }
    if(scaleWidth !== null && scaleHeight !== null) {
	    icon.scaledSize = new google.maps.Size(scaleHeight, scaleWidth); // scaled size
    }
    if(originX !== null && originY !== null) {
        icon.origin = new google.maps.Point(originX, originY); // origin
    }
    if(anchorX !== null && anchorY !== null) {
        icon.anchor = new google.maps.Point(anchorX, anchorY); // anchor
    }
    
    /* instanciate the marker itself */
    var marker;
    if (number == 0) {
        marker = {
            position: new google.maps.LatLng(lat, long), 
            map: map,
            icon : icon
        }
    }
    else {
        marker = {
            position: new google.maps.LatLng(lat, long),
            map: map,
            label: number.toString()
        }
    }
    /* take in count the specified options */
    if(optimized !== null) {
        marker.optimized = optimized;
    }
    if(title != null) {
        marker.title = title;
    }
    /* transform our marker into a google maps marker */
    var marker = new google.maps.Marker(marker); 
    
    /* if content has been defined, create an InfoWindow that will pop-up on click */
    if(content != '') {
        /* attach the infoWindow to the marker */
	    marker.infobulle = new google.maps.InfoWindow({
            content  : content, // contenu qui sera affiché
            visible: false,
            position: new google.maps.LatLng(lat, long),
            maxWidth: 500
        });

        /* display the infoWindow on click */
        google.maps.event.addListener(marker, 'click', function addInfoWindow(data){
            marker.infobulle.open(map, marker);
        });
    }
    google.maps.event.addListener(marker, 'click', action);
    /* the index wil be useful in case of removal of the marker's childs and roads */
    marker.index = n;
    if(adjust) {
        /* add marker to the zone */
        zoneMarkers.extend(marker.getPosition());
        map.fitBounds(zoneMarkers);
    }
    then();
    return marker;
};

/** create and place a marker on a map
 * @int n: index our the created marker
 * @map map: map where the marker will be placed
 * @float lat, long: coordinates of the marker on the map
 * @string url: url of the marker's image
 * @int height, width: size of the marker
 * @int originX, originY: placement of the marker's image in the layout
 * @int anchorX, anchorY: placement of the marker relative to the coordinates
 * @int scaleX, @scaleY: scaled size of the marker's image
 * @boolean optimized: true if all the markers have to be united in one graphic (better for time complexity, worse for resolution)
 * @string title: message to show onmouseover
 * @string content: content of the InfoWindow displayed on click, if empty, no InfoWindow
 * @function action: function to call on click in addition to the InfoWindow
 * @int number : number to place in the icon
 */
function createMarkerForNumber(n, map, lat, long, url = '', height = null, width = null, originX = null, originY = null, anchorX = null, anchorY = null, scaleHeight = null, scaleWidth = null, optimized = null, title = null, content = '', action, number) {
    return generateIcon(number, function(url) {
        return createMarker(n, map, lat, long, url, height, width, originX, originY, anchorX, anchorY, scaleHeight, scaleWidth, optimized, title, content, action);
    });
};

/** delete all the markers and lines/roads from a given array 
 * @array path: array of (markers , line/road)
 */
function deletePaths(path) {
    /* if we have a non-empty array */
    if(path != null && path.length > 0) {
        for(var k = 0; k < path.length; k++) {
            if(path[k]['marker'] != null) {
                deletePaths(paths[path[k]['marker'].index])
                /* remove the marker and road of the map */
                path[k]['marker'].setMap(null);
            }
            if(path[k]['road'] != null) {
                path[k].road.setMap(null);
            }
        }
        /* our path array will be an empty array */
        path = new Array();
    }
    return path;
}

/** display the childs marker of a marker, and handle their behaviour 
 * @int nb: number of childs marker to create and display
 * @float lat, long: coordinates of the father marker
 * @array[markers, roads] path: array containing all the paths for the father marker's childs
 */
function displayMarkers(nb = 5, lat, long, path) {
    if(path == null) {
        path = new Array();
    }
    /* if we already have markers childs, we delete them */
    if(path.length > 0) {
        path = deletePaths(path);
    }
    else {
        for(var k=0; k < nb; k++) {
            /* choose coordinates at random */
            var lat1 = Math.random() * width + lat - (width / 2);
            var long1 = Math.random() * height + long - (height / 2);
            /* instanciate a new couple (marker, road) */
            path[k] = new Array();
            /* if user chosen to display road or map, handle it */
            if(lineType == 'it') {
                path[k]['road'] = computePlotWay(lat+', '+long, lat1+','+long1);
            }
            else {
                path[k]['road'] = tracePolyline([{lat: lat, lng: long},{lat: lat1,lng: long1}], color);
            }

            /* if user wants that every child creates new markers, handle it */
            if(recursion) {
                path[k]['marker'] = createMarker(index, map, lat1, long1, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, '', '', function(){paths[this.index] = displayMarkers(number, this.getPosition().lat(), this.getPosition().lng(), paths[this.index]);});
            }
            else {
                path[k]['marker'] = createMarker(index, map, lat1, long1);
            }
            index++;
        }
    }
    return path;
};

/** determines if coordinates can be reached using the road that lead to them 
 * @google.maps.DirectionsRenderer road: road to the marker
 */
function hasValidDestination(road) {
    for(var d in road.gm_bindings_.directions) {
        for(var d2 in road.gm_bindings_.directions[d]) {
            if(road.gm_bindings_.directions[d][d2] == "directions") {
                for(var d3 in road.gm_bindings_.directions[d]) {
                    if(""+road.gm_bindings_.directions[d][d3] == "[object Object]") {
                        return road.gm_bindings_.directions[d][d3].enabled;
                    }
                }
            }
        }
    }
};

/** recursive function to plot a reachable marker
 * @int id: index of the marker to create
 * @float latO, longO: coordinates of the origine of the road
 * @float latD, longD: coordinates of the destination
 * @int i: index of couple (marker, road) in the paths' array
 */
function plotReachableMarker(id, latO, longO, latD, longD, i, path) {
    path[i] = new Array();
    path[i]['road'] = computePlotWayPlusMarker(latO, longO, latD, longD, i, function(j, x, y){
        /* create the wished marker */
        /* handle the choice of the user concerning the recursion */
        if(recursion) {
            path[j]['marker'] = createMarker(id, map, x, y, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, '', '', function(){
                paths[this.index] = displayMarkers2(number, this.getPosition().lat(), this.getPosition().lng(), paths[this.index]);
            });
        }
        else {
            path[j]['marker'] = createMarker(index, map, x, y);
        }

        /* if marker is not reachable, delete it, delete the road, and try it again */
        if(!hasValidDestination(path[j]['road'])) {
            console.log("pas bonnes coordonnées");
            deletePaths([path[j]]);
            plotReachableMarker(id, latO, longO, latD, longD, j, path);
        }
    });
};

/** display the childs marker of a marker, and handle their behaviour, avoid unreachable coordinates
 * @int nb: number of childs marker to create and display
 * @float lat, long: coordinates of the father marker
 * @array[markers, roads] path: array containing all the paths for the father marker's childs
 */
function displayMarkers2(nb = 5, lat, long, path) {
    if(path == null) {
        path = new Array();
    }
    /* if we already have markers childs, we delete them */
    if(path.length > 0) {
        path = deletePaths(path);
    }
    else {
        for(var k=0; k < nb; k++) {
            /* choose coordinates at random */
            var lat1 = Math.random() * width + lat - (width / 2);
            var long1 = Math.random() * height + long - (height / 2);
            /* instanciate a new couple (marker, road) */
            path[k] = new Array();
            /* handle the choice of the user concerning the type of lines */
            if(lineType == 'it') {
                plotReachableMarker(index, lat, long, lat1, long1, k, path);
            }
            else {
                /* if we do not compute and plot the road, we don't mind if it's reachable */
                path[k]['road'] = tracePolyline([{lat: lat, lng: long},{lat: lat1,lng: long1}], color);
                if(recursion) {
                    path[k]['marker'] = createMarker(index, map, lat1, long1, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, '', '', function(){paths[this.index] = displayMarkers2(number, this.getPosition().lat(), this.getPosition().lng(), paths[this.index]);});
                }
                else {
                    path[k]['marker'] = createMarker(index, map, lat1, long1);
                }
            }
            index++;
        }
    }
    return path;
};

/** compute and display the road between to markers
 * @string origin: 'lat, long', coordinates of the origin
 * @string destination: 'lat, long', coordinates of the destination
 * @google.maps.DirectionsTravelMode mode: travel mode to use for the itinerary
 * @boolean suppressMarker: true if we false to delete default markers A/B at each edge of the road
 * @string colorL: '#xxxxxx', the color of the line
 * @int weight: font-weight of the line
 * @DOMElement panelR: panel where to display the steps of the journey
 */
function computePlotWay(origin, destination, mode = travelModes[travelMode], suppressMarker = true, colorL = color, weight = 3, panelR = panel) {
    /* define the service and the display for our road, using the defined parameters */
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers: suppressMarker,
        suppressInfoWindows: true, 
        draggable: true,
        preserveViewport: true,
        polylineOptions : {
            strokeColor: colorL,
            strokeWeight: weight
        },
        panel : panelR
    });
    directionsService.route({
        origin: origin,
        destination: destination,
        travelMode: mode
    }, 
    function(response, status) {
        /* Route the directions and pass the response to a function to create markers for each step.*/
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
    return directionsDisplay;
}

/** compute and display the road between to markers, and display the destination's marker
 * @float latO, longO: coordinates of the origin
 * @float latD, longD: coordinates of the destination
 * @google.maps.DirectionsTravelMode mode: travel mode to use for the itinerary
 * @int i: index of the marker to plot
 * @function callback: function to call when road as been computed
 * @boolean suppressMarker: true if we false to delete default markers A/B at each edge of the road
 * @string colorL: '#xxxxxx', the color of the line
 * @int weight: font-weight of the line
 * @DOMElement panelR: panel where to display the steps of the journey
 */
function computePlotWayPlusMarker(latO, longO, latD, longD, i, callback, mode = travelModes[travelMode], suppressMarker = true, colorL = color, weight = 3, panelR = panel) {
    /* define the service and the display for our road, using the defined parameters */
    var directionsService = new google.maps.DirectionsService;
    var directionsDisplay = new google.maps.DirectionsRenderer({
        map: map,
        suppressMarkers: suppressMarker,
        suppressInfoWindows: true, 
        draggable: true,
        preserveViewport: true,
        polylineOptions : {
            strokeColor: colorL,
            strokeWeight: weight
        },
        panel : panelR
    });
    directionsService.route({
        origin: latO+','+longO,
        destination: latD+','+longD,
        travelMode: mode
    }, 
    function(response, status) {
        /* Route the directions and pass the response to a function to create markers for each step. */
        if (status === google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
            callback(i, latD, longD);
        }
    });
    return directionsDisplay;
}

/** display a line between two markers
 * @array[{lat, long}] coordinates: coordinates of all the markers to link with the line
 * @string color: color of the line
 * @float opacity: opacity of the line
 * @int weight: weight of the line
 */
function tracePolyline(coordinates, color = "#444444", opacity = 1.0, weight = 2) {
    var line = new google.maps.Polyline({
        path: coordinates,
        geodesic: true,
        strokeColor: color,
        strokeOpacity: opacity,
        strokeWeight: weight,
        map: map
    });
    return line;
};

/** create an display a map 
 * @DOMNode element: the element were to display the map
 * @float lat, long: coordinates of the center of the map
 * @int zoom: zoom on the map
 * @google.maps.MapTypeId type: type of map
 */
function createMap(element, lat, long, zoom, type = mapTypes[mapType]) {
    return new google.maps.Map(element, {
        'zoom': zoom,
        'center': new google.maps.LatLng(lat, long),
        'mapTypeId': type
    });
};

function resetMap(paths, markers, it, index) {
    /* if lines and markers are already present on the map, delete them */ 
    if(paths != null) {
        for(var k = 0; k < paths.length; k++) {
            paths[k] = deletePaths(paths[k]);
        }
    }
    if(markers != null && markers.length > 0) {
        for(var k = 0; k < markers.length; k++) {
            if(markers[k] != null) {
                markers[k].setMap(null);
                delete markers[k];
            }
        }
    }
    if(it != null) {
        it.setMap(null);
    }
    
    /* reset all constants of the map */
    index = 0;
    paths = new Array();
    markers = new Array();
    return paths, markers, index, it;
}
    
/*function initialize() {
    /* definition of the global variables */
    //var paths, markers, it, map, panel, direction, index = 0, number, numberZ, width, height, lineType, recursion, color, mapType, travelMode, centerLat, centerLong, widthZ, heightZ, coordinatesAdditionnalMarkers, zoneMarkers, adjust, displayItinerary;

    /** 
     * display all wished elements on the map, and handle their behaviour
     */
    /*function displayElements() {
        createMap(document.getElementById("div_karte"), 48.513202, 7.081958, 6);
        paths, markers, index, it = resetMap(paths, markers, it, index);
	    /* if additionnal markers wanted, handle it */
	    /*for(var i = 0; i < numberZ; i++) {
	        if(coordinatesAdditionnalMarkers[i] != null && (coordinatesAdditionnalMarkers[i].lat != '' || coordinatesAdditionnalMarkers[i].lng != '')) {
	            markers[index] = createMarker(index, map, coordinatesAdditionnalMarkers[i].lat, coordinatesAdditionnalMarkers[i].lng, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, '', 'Zusätzliche Marker', function(){paths[this.index] = displayMarkers2(number, this.getPosition().lat(), this.getPosition().lng(), paths[this.index]);});
	        }
	        else {
	            /* create a marker with random image at random position inside a zone */
	            /*markers[index] = createMarker(index, map, ((Math.random() * widthZ) + parseFloat(centerLat) - (widthZ/2)), ((Math.random() * height) + parseFloat(centerLong) - (heightZ/2)), '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, '', 'Zusätzliche Marker', function(){paths[this.index] = displayMarkers2(number, this.getPosition().lat(), this.getPosition().lng(), paths[this.index]);});
	        }
	        index++;
	    }
	};

	displayElements();
	handleCoordinatesAdditionnalMarkers();
	
	/* if the user wants to change a parameter, reload the map */
	/*document.getElementById('submit').addEventListener('click', function() {
	    refreshValues();
        displayElements();
    });
};*/
