var schools = [];

function plotStudents(school) {
    for(var j = 0 ; j < school.listOtherUnis.length ; j++) {
        if(markers[school.listOtherUnis[j].studentchoices__formation__university] == null){
            getSchool(school.listOtherUnis[j].studentchoices__formation__university, function(school2) {
                if(school2.lat != 0 || school2.lng != 0) {
                    markers[school2.id] = createMarker(school2.id, map, school2.lat, school2.lng, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, school2.name, school2.name, function(){plotStudents(schools[this.data.id]);}, school2.numberStudents, function(){
                    //plot lign to other coordinates
                    tracePolyline([{lat: school.lat, lng: school.lng}, {lat: school2.lat, lng: school2.lng}]);});
                    markers[school2.id].data = school2;
                }
            });
        }
        else {
            //plot lign to other coordinates
            tracePolyline([{lat: school.lat, lng: school.lng}, {lat: markers[school.listOtherUnis[j].studentchoices__formation__university].data.lat, lng: markers[school.listOtherUnis[j].studentchoices__formation__university].data.lng}]);
        }
    }
}

function plotSchools() {
    var paths, it, index;
    paths, markers, it, index = resetMap(null, markers, null, 0);
    schools = [];
    delete map;
    var lat = 0, long = 0, zoom = 5;
    map = createMap(document.getElementById("map"), lat, long, zoom);
    getSchools();
}

/**
 * for each school, place a marker on the map
 */
function displaySchools() {
    for(var i = 0; i < schools.length; i++) {
        if(schools[i] != null && (schools[i].lat != 0 || schools[i].lng != 0)) {
            markers[schools[i].id] = createMarker(schools[i].id, map, schools[i].lat, schools[i].lng, '', undefined, undefined, undefined, undefined, undefined, undefined, undefined, undefined, false, schools[i].name, schools[i].name, function(){plotStudents(schools[this.data.id]);}, schools[i].numberStudents);
            markers[schools[i].id].data = schools[i];
        }
    }
}

var urlMap = "http://localhost:8000/";

function getSchools() {
    $.ajax({
        type: 'GET',
        url: urlMap + "api/schools/",
        data: $("#form-filter").serialize(),
        dataType: "json",
        success: function(data) {
            for(var i = 0 ; i < data.length ; i++) {
                schools[data[i].id] = data[i];
            }
            displaySchools();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('echec');
            //console.log(xhr.responseText);
        }
    });
}
function getSchool(id, callback) {
    $.ajax({
        type: 'GET',
        url: urlMap + "api/schools/" + id + "/",
        dataType: "json",
        success: function(data) {
            schools[id] = data;
            callback(data);
        },
        error: function (xhr, ajaxOptions, thrownError) {
            console.log('echec');
            //console.log(xhr.responseText);
        }
    });
}

//document.getElementById("map").addEventListener('load', function() {console.log("coucou");});
google.maps.event.addDomListener( window, 'load', plotSchools);
