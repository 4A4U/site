from django.shortcuts import render
from django.core.serializers import serialize
from django.db.models import Q, Count, Case, When, IntegerField, Prefetch
from django.http import HttpResponse, JsonResponse
from db.models import Student, Formation, StudentChoice, StudentSatisfaction, Country, School, Question, StudentAnswer, CurrentStudent
from display.stat import get_score
from hashlib import sha1

def parse_pays(name, request):
    pays = list(map(int, request.POST.getlist(name, [])))
    pays_empty = len(pays) == 0
    countries = Country.objects.all()
    if pays_empty:
        pays = countries
    return [pays, pays_empty, countries]

def parse_categories(name, request):
    cats = request.POST.getlist(name, [])
    cats_empty = len(cats) == 0
    typ = []
    inter = 'Formation à l\'International'
    if not cats_empty and len(cats) < 6:
        if 'corps' in cats:
            typ.append('Corps d\'Etat')
        """ à décocher si on arrive à différentier les catégories
        if 'cat1' in cats:
            typ.append(inter)
        if 'cat2' in cats:
            if inter not in typ:
                typ.append(inter)
        if 'cat3' in cats:
            pays.extend(['ETATS-UNIS', 'JAPON', 'ROYAUME-UNI', 'CANADA', 'AUSTRALIE'])
            if inter not in typ:
                typ.append(inter) """
        if 'inter' in cats:
            typ.extend([inter, 'EV2 en double diplôme - Formation à l\'internationa...'])
        if 'ecole' in cats:
            typ.append('Écoles')
        if 'master' in cats:
            typ.extend(['Master Catalogue Hors X', 'Master-EP', 'Master Paris-Saclay', 'Master hors X'])
        if 'hc' in cats:
            typ.append('Hors Catalogue')
        if 'phd' in cats:
            typ.append('Phd-Doctorat')
    if len(typ) == 0:
        typ = ['Corps d\'Etat', 'EV2 en double diplôme - Formation à l\'internationa...', inter, 'Écoles', 'Master Catalogue Hors X', 'Master-EP', 'Master Paris-Saclay', 'Master hors X', 'Hors Catalogue', 'Phd-Doctorat']
    return [typ, cats, cats_empty]

def parse_PA(name, request):
    PA_chosen = request.POST.getlist(name, [])
    PA = Student.SUBJECTS
    if len(PA_chosen) == 0:
        PA_chosen = [pa[0] for pa in PA]
    return [PA_chosen, PA]

def index(request):
    formations =[]
    return render(request, 'display/accueil.html', locals())

def test(request):
    # calcul distance entre deux élèves pour les choix de cours
    # calcul distance entre deux élèves pour les matières
    #foreach formation :
    #    score = moyenne(1 / distance entre gens qui ont choisi cette formation et élève précisé, éventuellement pondéré par numéro/ratio du choix)

    formations = sorted(Formation.objects.all(), key=lambda a: a.score(student))
    formations_ratio = sorted(Formation.objects.all(), key=lambda a: a.score_ratio(student))
    formations_hamming = sorted(Formation.objects.all(), key=lambda a: a.score_hamming(student))
    formations_hamming_ratio = sorted(Formation.objects.all(), key=lambda a: a.score_hamming_ratio(student))
    formations_jaccard = sorted(Formation.objects.all(), key=lambda a: a.score_jaccard(student))
    formations_jaccard_ratio = sorted(Formation.objects.all(), key=lambda a: a.score_jaccard_ratio(student))

    return render(request, 'display/accueil.html', locals())

def test2(request):
    # calcul distance entre deux élèves pour les choix de cours
    # calcul distance entre deux élèves pour les matières
    #foreach formation :
    #    score = moyenne(1 / distance entre gens qui ont choisi cette formation et élève précisé, éventuellement pondéré par numéro/ratio du choix)
    if request.GET.get('student_id', None) != None:
        student = Student.objects.get(pk=request.GET['student_id'])
        distances = ['', '_ratio', '_hamming', '_hamming_ratio', '_jaccard', '_jaccard_ratio']
        d1 = request.GET.get('distance1', '').replace('formations', '')
        d2 = request.GET.get('distance2', '').replace('formations', '')
        nb_resultats = int(request.GET.get('nb_resultats', ''))
        if d1 in distances and d2 in distances:
            [pays_coches, pays_coches_empty, liste_pays] = parse_pays('lieu', request)
            [typ, cats, cats_empty] = parse_categories('categorie', request)
            distance1 = sorted(Formation.objects.filter(Q(type__in=typ)&Q(university__country__in=pays_coches)), key=lambda a: getattr(a, "score{0}".format(d1))(student), reverse=True)[:nb_resultats]
            distance2 = sorted(Formation.objects.filter(Q(type__in=typ)&Q(university__country__name__in=pays_coches)), key=lambda a: getattr(a, "score{0}".format(d2))(student), reverse=True)[:nb_resultats]
        else:
            return render(request, 'display/error.html', locals())
    else:
        return render(request, 'display/error.html', locals())
    for formation in distance1:
        formation.score = getattr(formation, "score{0}".format(d1))(student)
        formation.nb_students = len(formation.studentchoices.all())
    for formation in distance2:
        formation.score = getattr(formation, "score{0}".format(d2))(student)
        formation.nb_students = len(formation.studentchoices.all())
    return render(request, 'display/conseils_4A.html', locals())

def conseils_4A(request, student_id=None, nb_results=20):
    [pays_coches, pays_coches_empty, liste_pays] = parse_pays('lieu', request)
    [typ, cats, cats_empty] = parse_categories('categorie', request)
    d1 = '_jaccard_ratio'
    nb_resultats = int(request.POST.get('nb_resultats', nb_results))
    student_id = request.GET.get('student_id', student_id)
    if student_id != None:
        #cas d'un student_id anonymise
        student = Student.objects.get(pk=student_id)
    else:
        #cas du vrai eleve
        student = CurrentStudent.objects.get(name=sha1(request.user.username.encode('ascii')).hexdigest())
    distance1 = sorted(Formation.objects.filter(Q(type__in=typ)&Q(university__country__in=pays_coches)), key=lambda a: getattr(a, "score{0}".format(d1))(student), reverse=True)[:nb_resultats]
    for formation in distance1:
        formation.score = getattr(formation, "score{0}".format(d1))(student)
        formation.nb_students = len(formation.studentchoices.all())
    return render(request, 'display/conseils_4A.html', locals())

def testNoCompute(request, student_id=None, nb_resultats=20):
    # ex: http://localhost:8000/testNoCompute?student_id=2&distance1=jaccard_9_0.15&distance2=jaccard_9_0.2
    student_id = request.GET.get('student_id', student_id)
    if student_id != None:
        #d,eD,eS,score =['jaccard','jaccard'],[9,9],[0.15,0.15],[[],[]]
        d,eD,eS,score ='jaccard',9,0.15,[]
        [pays_coches, pays_coches_empty, liste_pays] = parse_pays('lieu', request)
        [typ, cats, cats_empty] = parse_categories('categorie', request)
        student = Student.objects.get(pk=student_id)
        d = request.GET.get('distance1', '').split('_')
        if len(d)==3:
            eD=int(d[1])
            eS=float(d[2])
        score= student.sort_formation(exp_dist=eD,exp_stud=eS,only_under=True)
        nb_resultats = int(request.GET.get('nb_resultats', nb_resultats))
        duree = request.POST.getlist('duree', [])
        distance1 = sorted(Formation.objects.filter(Q(type__in=typ)&Q(university__country__in=pays_coches)), key=lambda a: score[a.id-1], reverse=True)[:nb_resultats]
    else:
        return render(request, 'display/error.html', locals())
    scalingfactor = rescaleFactor(score[distance1[0].id-1])
    for formation in distance1:
        formation.score = score[formation.id-1]*scalingfactor
        formation.nb_students = len(formation.studentchoices.all())
    return render(request, 'display/conseils_4A.html', locals())

def rescaleFactor(value):
    factor=1
    if value<=0:
        return 1
    else:
        while factor*value<1:
            factor=10*factor
        return factor/10

def testModel(request):
    #TODO @Plays run the test for all students and compute the error
    #TODO @Plays Change the template target
     # calcul distance entre deux élèves pour les choix de cours
    # calcul distance entre deux élèves pour les matières
    #foreach formation :
    #    score = moyenne(1 / distance entre gens qui ont choisi cette formation et élève précisé, éventuellement pondéré par numéro/ratio du choix)
    if request.GET.get('student_id', None) != None:
        student = Student.objects.get(pk=request.GET['student_id'])
    else:
        return render(request, 'display/error.html', locals())
    score = getscore(student)
    print(score)
    formations = sorted(Formation.objects.all(), key=lambda a: score[a.id-1], reverse=True)
    formations_ratio = sorted(Formation.objects.all(), key=lambda a: score[a.id-1],reverse=True)
    print(formations[0])
    return render(request, 'display/conseils_4A.html', locals())

def home(request, student_id=None):
    student_id = request.GET.get('student_id', student_id)
    if student_id != None:
        student = Student.objects.get(pk=student_id)
    else:
        student = CurrentStudent.objects.get(name=sha1(request.user.username.encode('ascii')).hexdigest())
    return render(request, 'display/home.html', locals())

def retours_4A(request, student_id=None):
    pays = list(map(int, request.POST.getlist('lieu', [])))
    pays_empty = len(pays) == 0
    countries = Country.objects.all()
    liste_universites = School.objects.filter(studentsatisfaction__isnull=False).distinct()
    colonnes_cochees = list(map(int, request.POST.getlist('colonne', [])))
    colonnes_cochees_empty = (len(colonnes_cochees)==0)
    universites_cochees = list(map(int, request.POST.getlist('universite', [])))
    universites_cochees_empty = (len(universites_cochees)==0)
    is_empty = pays_empty and universites_cochees_empty


    if pays_empty and universites_cochees_empty:
        universites_cochees = liste_universites
        pays = countries

    liste_des_questions = Question.objects.all()
    #if colonnes_cochees_empty:
    #    questions_wanted = liste_des_questions.values_list('id', flat=True)
    #else:
    #    questions_wanted = list(filter(lambda x: x >= 0, map(lambda x: (x-6), colonnes_cochees)))
    results = StudentSatisfaction.objects.prefetch_related(Prefetch('studentanswer_set', to_attr='answer_to_questions', queryset=StudentAnswer.objects.all())).filter(Q(country__in=pays)|Q(university__in=universites_cochees))
    print(len(results))
    # what you get :
    #   - country
    #   - university
    #   - duration
    #   - faculty (faculté de rattachement)
    #   - degree_type (master/phd/other)
    #   - degree_name (name of the formation)
    #   - satisfaction questions : t = results[0].studentanswer_set.all()
    #      - t[0].question.question = intitulé de la question
    #      - t[0].answer = réponse de l'étudiant
    # possibility of filtering on the response of students to a certain question : results = StudentSatisfaction.prefetch_related('studentanswer_set')
    liste_des_colonnes = [[0, 'Pays', True],[1, 'Université', True],[2, 'Durée', True],[3, 'Faculté de rattachement', True],[4, 'Type de diplôme', True],[5, 'Nom de la formation', True]]
    i = 6
    for q in liste_des_questions:
        liste_des_colonnes.append([i, q.question, False])
        i = i + 1

    if colonnes_cochees_empty:
        colonnes_cochees = [c[0] for c in liste_des_colonnes]
    return render(request, 'display/retours_4A.html', locals())

def fake_login(request):
    return render(request, 'display/login.html', locals())
    error = student.error()
    errorPosition = student.error_position()
    return render(request, 'display/accueil.html', locals())

def display_nice_SOIE(request):
    country = None
    university = None
    duration = None #in months
    results = StudentSatisfaction.objects.filter(country=country, university=university, duration=duration)
    # what you get :
    #   - country
    #   - university
    #   - duration
    #   - faculty (faculté de rattachement)
    #   - degree_type (master/phd/other)
    #   - degree_name (name of the formation)
    #   - satisfaction questions : t = results[0].studentanswer_set.all()
    #      - t[0].question.question = intitulé de la question
    #      - t[0].answer = réponse de l'étudiant
    # possibility of filtering on the response of students to a certain question : results = StudentSatisfaction.prefetch_related('studentanswer_set')
    return render(request, 'display/accueil.html', locals()) #TODO : change template

def nb_students_per_school(request, student_id=None):
    [PA_chosen, PA] = parse_PA('PA', request)
    student_id = request.GET.get('student_id', student_id)
    [pays, pays_empty, countries] = parse_pays('lieu', request)
    [typ, cats, cats_empty] = parse_categories('categorie', request)
    results = School.objects.filter(formations__studentchoices__student__pa__in=PA_chosen, country__in=pays, formations__type__in=typ).annotate(students_count=Count('formations__studentchoices__student')).annotate(**{"students_{0}".format(pa): Count(Case(When(formations__studentchoices__student__pa=pa, then=1), output_field=IntegerField)) for pa in PA_chosen }).order_by("-students_count")
    return render(request, 'display/stats.html', locals())

def mapView(request, student_id=None):
    [PA_chosen, PA] = parse_PA('PA', request)
    student_id = request.GET.get('student_id', student_id)
    [pays, pays_empty, countries] = parse_pays('lieu', request)
    [typ, cats, cats_empty] = parse_categories('categorie', request)
    return render(request, 'display/map.html', locals())

def schools(request):
    [PA_chosen, PA] = parse_PA('PA', request)
    [pays, pays_empty, countries] = parse_pays('lieu', request)
    [typ, cats, cats_empty] = parse_categories('categorie', request)
    results = School.objects.prefetch_related('formations', 'formations__studentchoices', 'formations__studentchoices__student').filter(formation__studentchoice__student__pa__in=PA_chosen, country__in=pays, formation__type__in=typ).annotate(students_count=Count('formation__studentchoice__student')).annotate(**{"students_{0}".format(pa): Count(Case(When(formation__studentchoice__student__pa=pa, then=1), output_field=IntegerField)) for pa in PA_chosen }).order_by("-students_count")
    data = serialize('json', results)
    return HttpResponse(data)
