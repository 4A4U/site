P3A - IBM - Orientation proposals
================================

Installation (standalone version)
---------------------------------

**Requirements.**
On Debian-based distribution, you will need the following librairies:
```shell
sudo apt-get install python-dev libmysqlclient-dev libjpeg-dev python3.5-dev
```

**Python 3 is required.**
We recommend to use a virtualenv for python. If you have python3 installed, it should had come with it.
After you have cloned the repository, you must execute the following commands in your shell:
```shell
cd /path/to/folder
virtualenv --python=python3 .env
source .env/bin/activate
pip install -r requirements.txt
echo "***" > mdp_mysql #"***" should be replaced by "mdp_for_mysql_for root_user"
./manage.py runserver
```

If you don't want to use a virtualenv, follow these instructions to setup the project.

Install requirements
`pip install -r requirements.txt`

If you have just made a `git pull`, migrate the database with
`python manage.py migrate`

Run dev server
`python manage.py runserver`

Website is accessible at `127.0.0.1:8000`.
