
--
-- Contenu de la table `db_country`
--

INSERT INTO `db_country` (`id`, `name`) VALUES
(1, 'FRANCE'),
(2, 'ETATS-UNIS'),
(3, 'CANADA'),
(4, 'ROYAUME-UNI'),
(5, 'SUISSE'),
(6, 'ITALIE'),
(7, 'SUÈDE'),
(8, 'PAYS-BAS'),
(9, 'ALLEMAGNE'),
(10, 'SINGAPOUR'),
(11, 'CHINE'),
(12, 'JAPON'),
(13, 'AUTRICHE'),
(14, 'ISRAËL'),
(15, 'CHILI'),
(16, 'BRÉSIL'),
(17, 'AUSTRALIE'),
(18, 'ESPAGNE'),
(19, 'NORVÈGE'),
(20, 'DANEMARK'),
(21, 'RUSSIE, FÉDÉRATION DE'),
(22, 'TURQUIE'),
(23, 'HONG KONG'),
(24, 'IRLANDE');
